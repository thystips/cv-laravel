# Changelog

<!--- next entry here -->

## 1.1.0-master.3
2020-01-23

### Fixes

- **header boad:** residues (c9b3fac070bce8182ad7e177d799bf944ea68fd2)
- **merges:** release to master (008fa326c710bdd0df8c77ef688bdd923674c6d6)

## 1.1.0-master.1
2020-01-23

### Features

- **style:** rookie mistake (abf62f4210872a2efb5c5a110a271bb0e3bb4ec0)

### Fixes

- **permission:** hide permission on error (3ce8628cbcbc8093b4be2cddcc2df30d7da191df)

## 1.0.0-master.11
2020-01-23

### Fixes

- **extensions:** Add extension (c6e8de31b7a5efc014683373f6d93c5944543552)

## 1.0.0-master.10
2020-01-23

### Features

- **style:** Mobile nav position absolute to fixed (rookie mistake) (be5d15b979c8df02a598afc0f1bbd72cb6cac5eb)

## 1.0.0-master.9
2020-01-22

### Fixes

- **??:** 474 pull requests to do ?? (737773709e5cf10d16531587be5db354fb924dc1)

## 1.0.0-master.8
2020-01-21

### Features

- **style:** Fully responsive user-board and header board + user header navigation (4e9d8ee2a0d7c5fd23a726ab228cc6d315ffc354)

### Fixes

- **information view:** still dd written (f8384f90e3323669c993663cc409572deb5beb68)
- **style - forms:** alert text should be center-aligned (0051b14726d08deaa5b03c5186a64f05a175818a)

## 1.0.0-master.6
2020-01-21

### Features

- **style:** add responsive fonctionnalities (42c2662d5dc771c47355ccb6738eb1ace6640c0e)
- **styles:** Responsive Navbar + Forms (login, register, verify, confirm) (f1555cf7d5a870d0eaaddb080b3ea53ec3bbb822)
- **permission:** Permissions problems (6ecef5bec14796ecc9cd221db3c38dc6c4113648)
- **style:** Letter spacing hover responsive global menu (d9ade20d9715c699e6ba527d8206b2669eaa7a55)
- **authentication:** add login with Username OR Email (5f790e6c80c571cf0faf2d2eb619eca782728d06)
- **style:** User Navbar responsive complete (4ff6b2ef46725bf7eee433d5c3e22739bc75ddbc)
- **composer:** composer update (7fda2178ae5c87c99630efdf81353fcfce04182c)

### Fixes

- Update composer (7a84d279714e49f2a38123fe3da4114f00f63c67)

## 1.0.0-master.4
2020-01-21

### Features

- **style:** add responsive fonctionnalities (42c2662d5dc771c47355ccb6738eb1ace6640c0e)
- **styles:** Responsive Navbar + Forms (login, register, verify, confirm) (f1555cf7d5a870d0eaaddb080b3ea53ec3bbb822)
- **permission:** Permissions problems (6ecef5bec14796ecc9cd221db3c38dc6c4113648)
- **style:** Letter spacing hover responsive global menu (d9ade20d9715c699e6ba527d8206b2669eaa7a55)

### Fixes

- Update composer (7a84d279714e49f2a38123fe3da4114f00f63c67)

## 1.0.0-master.3
2020-01-21

### Features

- **style:** add responsive fonctionnalities (42c2662d5dc771c47355ccb6738eb1ace6640c0e)
- **styles:** Responsive Navbar + Forms (login, register, verify, confirm) (f1555cf7d5a870d0eaaddb080b3ea53ec3bbb822)
- **permission:** Permissions problems (6ecef5bec14796ecc9cd221db3c38dc6c4113648)

### Fixes

- Update composer (7a84d279714e49f2a38123fe3da4114f00f63c67)

## 1.0.0-master.2
2020-01-16

### Features

- **style:** add responsive fonctionnalities (42c2662d5dc771c47355ccb6738eb1ace6640c0e)

### Fixes

- Update composer (7a84d279714e49f2a38123fe3da4114f00f63c67)
- **permissions:** Repair permissions middlware name (fae935674d8636cd8cd9d76826baabe03ac0c298)

## 1.0.0-master.1
2020-01-16

### Breaking changes

#### **project:** Add how deploy app (548099aae1898dae7a048bae3da7d36c988410e8)

New larecipe documentation

### Features

- **package:** Update Composer and NPM packages (9b2b2457b6b410270760cc97975b41ccea631a0e)
- **doc:** dark theme + caching (986b24394b49a4439bdfe5af2e6fd2a08d8f4357)
- **seeding:** disable user creation on seeding (db3b80989ef37241d33bb5fbf543a1b6450a3b2b)

### Fixes

- clean repo (19007a56cd79e966b679b777e14dc45717aa7d66)
- clean .env for production (d294e91f7985d8e51b2b37173b0d396c0980dda3)

## 0.11.0-master.16
2019-12-03

### Features

- **avatar:** Creating collection (23a0603910e95c49d6820eb1f59b28d195fcf449)
- **experiences:** add button to set if exp is still current (e35742adf7c2e98c691c519b3f0ccb6fc63e2772)
- **avatar storage:** Change library folder (411c16ca72ca32898084a451372a1ec4672aaeba)

### Fixes

- **avatar:** Fix storage (5bc6d2c0897961911bb1650b03da00b931f16328)
- **board:** better UI UX (a87492e0e0c93572e943483cff56dd6714e6b776)

## 0.11.0-master.14
2019-12-03

### Fixes

- **avatar:** Fix storage (5bc6d2c0897961911bb1650b03da00b931f16328)

## 0.11.0-master.13
2019-12-03

### Features

- **informations avatar:** add display avatar on profile page (d2c55e048f122e1ed374384d40bba885457ca7ba)
- **iformations:** add responsive images to collection (0493a323251c48842247030b02197efde234fb81)
- **profile:** display avatar image on profile page (665ded3b3f908b00c6026714e4ad79412a7778c8)

### Fixes

- **avatar:** Add avatar in index method (0aea1b78b07995905fd3713d265df7bc55bfd880)
- **avatar:** fix avatar upload (7f112d195fdc85631e37e8845988d73cc5f6c77f)

## 0.11.0-master.12
2019-12-03

### Features

- **informations avatar:** add display avatar on profile page (d2c55e048f122e1ed374384d40bba885457ca7ba)

### Fixes

- **avatar:** Add avatar in index method (0aea1b78b07995905fd3713d265df7bc55bfd880)
- **avatar:** fix avatar upload (7f112d195fdc85631e37e8845988d73cc5f6c77f)

## 0.11.0-master.10
2019-12-03

### Features

- **avatar:** Avatar's storag on spatie media library (44dab4332efdebd5ee9aa113b36c1a51c24d2628)

## 0.11.0-master.9
2019-12-03

### Features

- **style:** disabled checkbox accout infos (57255bde60f646faede852e4ebaa38ec5bb6f8a9)
- **informations:** add file input profile page for avatar (73ef912f9f793fae4d1d7e492c22c75e192bf8fb)

## 0.11.0-master.8
2019-12-03

### Features

- **avatar:** Add avatar (a1c606a2bfef2f90a49b695a15043168bbc22e8c)

## 0.11.0-master.7
2019-12-03

### Features

- **avatar:** Add upload for avatar in InformationController (43cbce7eac743e6f710491d728fef9619589daab)

## 0.11.0-master.6
2019-12-03

### Fixes

- **informations:** Update method edit to edit right values in table (32e2fd39a971cdb7085efae49ee15560a129b247)

## 0.11.0-master.5
2019-12-03

### Fixes

- **usettemplate:** change usetmplate routes for resource (2b5cd32e36ae8a11b0b407274496ca17ced84627)
- **usertemplate:** fix controller update method (d52dd9ab7b34d1b1ca109a891c59f8d7c1f4d5ab)
- **customcv:** fix retour button and print errors (ab852efe4f21d8b49fc326d3348294686eb1221b)
- **customcv:** update method (0531d617d27dadee9d0e5f261b41ebaab8ddf078)
- **showcv:** fix route to show cv (4044e64bb83a22a7f557ba0837328ac1992f465b)
- **customcv:** link relation user to foreingn key customcv table (bf6f65f3bfb0154aa66844a61c25ea792f9d4412)

## 0.11.0-master.3
2019-12-03

### Fixes

- **usettemplate:** change usetmplate routes for resource (2b5cd32e36ae8a11b0b407274496ca17ced84627)
- **usertemplate:** fix controller update method (d52dd9ab7b34d1b1ca109a891c59f8d7c1f4d5ab)
- **customcv:** fix retour button and print errors (ab852efe4f21d8b49fc326d3348294686eb1221b)
- **customcv:** update method (0531d617d27dadee9d0e5f261b41ebaab8ddf078)

## 0.11.0-master.2
2019-12-03

### Features

- **usertemplate:** added custom routes for user template (e4112bbd2da767efa6d49b79b86ddd71ae588398)

### Fixes

- **user model:** Set return type to VOID for user template setter (04859dd39948b06c264723774cc56639079cf7f6)
- **usertemplatecontroller:** test using resource instead of hand created controller (might works better) (255aacebfecba3e30ba16ddeddd029f67a70d94c)
- **packages:** Update dependencies (eaa2848c06c66e7937fc58614269f3d7c0a8e6e6)
- **admin:** fix rights on default admin account (7116d32ac982d2ce7c8a8c9e19c2d7339b2153cb)
- **gitlab-ci:** Change versioning) (f4af7127cbd571f738e5c0f6d56ac71ac2832383)
- **user:** try fix template_id on user model (f34abb77f4f063c6bf349b43c6cef98eb6c0540a)

## 0.10.0
2019-12-02

### Features

- **customcv:** User using Custom CV can only access do associate section (8733e6062fd89f2905afd683004a4ce8bf632a3b)
- **style:** add margin for row alone (7740a0254a6c7823eaafdd599a01e26ff4d05fdd)
- **customcv:** add Route, Download function and button to download current uploaded CV (e88ff527edef1088dacb1733e30f10b26b393574)
- **customcv:** add view customcv show (53d79aa7826d1e16af32fdcdf8f69327126742d7)
- **customcv:** add show btn customcv pdf (b148aaefffbfc27ebbb58db4e7779dee3ab52825)
- **auth:** verify email page finished (8d06375da146c22f64520292d85b9e51189e1fa7)
- **email verify:** Email verification needed (c938475d6b83d1b930924db3d3662fa6ce845ee9)
- **customcv:** Finished public show of CV (89f01538631a3b9232d2b6600a33ff472c3c1e6f)
- **template-1:** Adding ressources for template 1 (550018123090ce22a91cc34fffba01c3dcea89bf)
- **show cv:** Show cv button in user header (8b9c957a8e3c4e19c132c4ab5e3b61e8c1576d79)
- **role:** Roles, Permissions and seeding (826f62672b662fec45b27a259a312dfcf46fe291)
- **cvdisplay:** Redirect to online cv if custom cv is OFF (e55462b0d5dc780681348fbb19c7090d363c0931)
- **cvdisplay:** display cv online page (2ba5205868fcf20fd53ccd1ee2d78eafb70d1306)
- **cvdisplay:** Display online cv page (c7df2e5680c7cebbc66efd9aa405687946961c4f)
- **header view:** add index route for logo in header (f63eb5c492fefa6c95edfba621cd5f802d311ff9)
- **userheaserview:** add index route to logo (9f6d0f12a0a3788d89eae50e48f1066eeb9df5b2)
- **routes:** add resources fo cvcontroller (eef3d6faf7209dc8d89e3e82368616bdf66fef49)
- **template-1:** Finished template 1 online CV (36f226ffede82f6ac8c8839363a3a2b332c25bc7)
- **role:** Admin permissions (c0b694cc3d253d33628d258d2571562a23952e22)
- **template-1:** Middle finished page template-1 (a8ddc6ed69203c45d8deb135e1af328d805a7862)
- **role:** Change roles for permissions + roles (62a9f19f219d6c97f07cc0cf453882fdaada3ad2)
- **index header view:** add logout button on index page header (877b1341996f417c24a6a6213fd1a06bd3c4d617)
- **template:** Add template controller, migration, model (158a4f5d4373cb94a207445644948d7eab25f3d6)
- **template view:** Create view using template (02aecc934e2e5361ab87316b5675cc35d4abb38e)
- **template view:** Add template page to board header (2a5de9ac1080937e25f0af975f1b1bbad3a6f62d)
- **routes:** add resources routes for template views (ca90e75878da783e28dd4fb9ccafa94c172af05d)
- **template:** UserTemplateController (d88310a74eebb0826cab09e3f7b9c5b1f0c40f92)
- **template:** UserTemplate (21057cd0a4d57ef77b3594e39ef36ecd7c1851f0)
- **usertemplacecontroller:** add user template controller to routes (24cec7f8eaf2ca68aee319ce5f9be04cf09fd50a)
- **template user:** fixed some issues (5337883b150430fca5876c5d3e5bc5b2ff0b393b)

### Fixes

- **ide_helper:** Refacto on ide helper (4bc7de13de222205d8bba4479701003d720aaa07)
- **routes:** Refacto on routes (23ef57d4b8acab3534a9aa84bceee386e78d2858)
- **composer:** Composer update (9011efdd19c6646f3ae7694ace4aeb278ed2a3c7)
- **customcv:** Conditionnal download button (76baf25d6a7fe2dc25b931227854c0227c909b5f)
- **cv:** Add CustomCv in CvController (38405d9057a8f4a24881eb3b573956efac52f161)
- **style:** Boarder radius Secondary btn (fbf04e8bda39cfebc38ec18b8f450910ee6de300)
- **email verify:** fix email verify blade (3a34b4779de696b32d8b797c1e7c60df5ef8046f)
- **laravel:** Update Laravel's packages and restor Bootstrap (52465c839db885e33f1758dfc3383e8df4c6fe4a)
- **ide_helper:** Update _ide_helper.php for PHPStorm (2bef82709318e73de2c80693fb359d95e7a968f0)
- **auth:** Verify email uppercase button (0d51596b19585d5c1eb4464c4b6c3e076c340437)
- **seeder:** Refactor RolesAndPermissionsSeeder (7825a0a49241e5c64fa87f5c21d3a09c9688f503)
- **seeder:** Complete UserTableSeeder (1144db3635c0a5fb2640c589f24d55bc009736b2)
- **cvcustom:** change routes on button (e2e284dd1aa0a35f5d820f50c8380f6770fe3e05)
- **composer:** Composer update (97eb74bb62a68acc57bcff615e199b8090b46c2e)
- **storage path:** Fix errors due storage path (88fff0022006aa8fd1343828b69f7a4a2b7416a8)
- **gittignore:** update gitignore back to original (4742b2ef0d5686a6186601061e6b8c5ca131e357)
- **role:** Add middleware for user role (50dbade12eaf648ff93b00b850ddd4367096cabb)
- **competences:** Add boolean for type (08743e6372d6853629674c629e5f660b747b704b)
- **cvcontroller:** no send customcv for online CV (236ba61ebfc62b76834a77c2c3fb5299116a2c1c)
- **style template-1:** prettier code (c51bed5ea52187cd951631eea00c214380095d7f)
- **seeder:** fix boolean in factory (66c2c4f102186f42d6d7304ce3c7df39e55c1a06)
- **composer:** composer update (dd493a734dade630a12fa4dc0da89ced008e0380)
- **templatecontroller:** Add use Auth (fa64c887172cde7e30b939d0b8b298c2ba229dfc)
- **informations view:** change argument for auth id (4bacedf1418343f6591fc05b0b38cc0cf8daf7c6)
- **template model:** No link (5bc56a5738b67e0daefa9c83afad9586a3efc569)
- **user modle -> template:** delete doc return (9742e27688159c69f7735c838d907a9e49e6427b)
- **templatecontroller:** No use of index (c373726112319bfdc50083fd5cdfa1b5a984378f)
- **usertemplatecontroller:** change passed varibale (04920cf5e3e9633127cf88b779e4233b99553089)
- **template:** Add Template Seeder (a5b011fe4d1854e50e8fcd76dfd1922f96f4946e)
- **template:** Fix Template Factory and Seeder (3eba397bbde105102a1e84edd41d50f86a60f17d)
- **templatecontroller & seeder:** use Template Model class directely (af14006a2140396060f3b47b05fe757ec83986c6)
- **seeder:** Template seeder (caabf8d1cd614a6603885d40ab0b6f05cc0d66e0)
- **tests:** fix url in phpunit tests (ba4f6d20ebccd40327ef1db4af35546b2c64249a)
- **template:** changed route to template user board view (231b9b4d3fc7062e31ac38a059d9ca39d56e2015)
- **templateseeder:** class not seeded and composer update (7f39b6ee62797d1390c0cd5811c85e6d4280cb19)
- **packages:** fix error due to yarn.lock (50b5a228ce7c77491e6b9964c00db99fb221c5c0)

## 0.9.1
2019-11-30

### Fixes

- **app:** PHPDoc String + return type + other refacto (29d316156cb4e45f515c3727530bc2ab409e7531)

## 0.9.0
2019-11-29

### Features

- **customcv:** add custom cv middleware (0a37a9a75f04b5c9ecd6dd922e70c3320d64d07b)

## 0.8.0
2019-11-29

### Features

- **style:** Add inactive board header. (40fb5c0c2ca9bd8aa44bd770b1cf5e924dab5d97)
- **customcv:** restrictions conditions about custom cv upload (e7a5c3405f49123840a6e312f95829aab3e87859)
- **mail:** Default mail provider mailgun (b34282010e836d6ce31e8dffd73e5fc029ed74e4)

### Fixes

- **account infos:** delete var_dump(s) on pages index and edit (dcac6f332233fbc33a2b9a27e91b9eed8d75f57f)
- **ignore:** ignoring phpstorm files (0977669cf6b2eeac1ed2fa9f017e3b97f2d97efc)
- **ide:** Start using phpstorm (1d112a690712b1799b4ee40c41d88b898d4c58d4)
- **docstring:** Auto add docstring (156357b8dc3a04e06d472aa514d561b15b2fe7ae)
- **composer:** Composer update (6e0e4026564a4a76195db58388b6578bc91c4a88)

## 0.7.0
2019-11-29

### Features

- **customcvcontroller:** finished edit page for custom cv (f9cfb01fc21308812dbeb4a5b4164c0026e30806)
- **email verify:** Enable email verify (cccac941d2fc7f02f310a7f8e9eeab5b461b42de)

### Fixes

- **customcv:** Fixing upload custom CV (419a3070f1c5df28990a973cff9ffa4c9de92b55)

## 0.6.0
2019-11-29

### Features

- **customcv:** Add index method (e79f3f730cd43c526be9f1fedfa4b67e865cd54a)
- **custom cv board and style:** add board page for custom cv upload. (e527faf52f71afa54768ac999eb159f847029fc2)

### Fixes

- **gitlab-ci:** deploy to dev on manual (13048335c3c77f58e7235ebedb331ad0ffda0bf4)
- **customcv:** index method (f6485796116411d2c39370f2a0dcf462c2d883ab)
- **customcv:** rules for accout edit page (aa4c5e18b826ef437c6111ff4e20dda11def9126)

## 0.5.0
2019-11-29

### Features

- **style:** add warning button (434190f4d96327a9c9cfb9b97b8246d15ea9b96c)
- **all views:** add all return button (4a735bc29255f6a084ca5fa33e70b4c42b4b0ffb)
- **experiences:** change input text to texarea (c0f2f4f435a28818cb10e4ceed6c7f84ff38c902)
- **socials:** finished crud for socials (cea141cb523e31e355c30eb823c8c3c9a866324e)
- **account:** Informations account page show and edit done. (ce7fe1296960db2e67c88a521f94979db98ccf6b)

### Fixes

- **all board pages:** reorganization of views directory (5a5cc8a20ed42fc64f21b5bd37b408264da9a953)
- **customcv:** Custom CV upload backend (3c2d9a49fd5082df25d78725d7a71d4c5a7cc590)
- **customcv:** fix registration issue (93b396482821ba451d22de3b3e049b6e353cb587)
- **customcv:** fix path to file (65d8740c0431f7fe5a27b716e06983cab9d6ace8)

## 0.4.0
2019-11-28

### Features

- **experiences:** update form add (016da99f1d6c78b8849c0dc13aa1a0f198c72675)
- **compétences:** Add full update form (f698a7ae16eabcbc6fd1acfd369949eaaeba1423)

### Fixes

- **gitlab-ci:** error from route:cache (22bac55b6685aadf7c51fe6ba7fbb044634303fe)
- **base:** composer update (b2d3e314182a3cbe8528e6a4f463a577bbd2a687)
- yarn update (2c1be624d380cf8e146d822afe4e7e9aa5910676)
- **blade:** Change social blade name (e81ebc3836d84d3a6ba8409dafd1aaed3b1fff20)
- **socials:** fix social routes (f1c87b08d812413922c96a3168ac5a46edb5929a)
- **informations:** Add email in Informations table (495ccaafebb4c96f9321031a8fa669683b3fc8c5)
- **various:** Style and controller register validatormessages (6db04dc06a52912394867a344f16dc1591d2e793)
- **informations:** my bad. (08f150faa812627c657f3b131442c27a5a5c2975)
- **informations:** fix update not working (d5effff7c3f03754c07b077c8749c6f30e7dd5f8)
- **controller:** Update method (ec097ea835ae9f078f1ba561aab7716622092808)
- **controller:** Update method (0747e90f89a59164ea018ed795cbf93a90efe8fb)
- **controller:** update method (d90dad531de334eaaf03a8ed09597f71df261778)

## 0.2.0
2019-11-25

### Features

- **compétences:** Finished style form add competences (be439124671050efdf02216cff8640089ebbbd92)
- **competences:** End compétences controller (1171ed458d9103cb8c26283977d07534cf2f31a8)

### Fixes

- **controller method:** Repair existing methodes in all customs controllers (b685acfcd63ac6020cb1230cdaab00263a7f10e1)
- **method:** Repair and rename functions in controllers and models (02d41ac6d05625491adb8570e408539c6e7dda1b)
- **competences:** findwhere ->find (bd2750c8bbceb0953dbc5709fc1b9fd070197ba6)
- **all tables:** Foreign key = user_id (given) (17ed493be3fbaaadfb0b86a2ae4767ac234bb95a)
- **migrations:** delete remembertoken (bb4873c21d75c8c673deba80dbcaa772ecf2d429)
- **model:** Delete IDs in model (ab96f931e3a6513bb183a6dc23259c9336babd97)
- **controller:** Repair controller method store (38e2c496f0348c4c16ee66599af9bd79f8c0c413)
- **competences:** get user competences (d44636323ad49968111e2bd1f60e1b76ff23b8ff)
- **controller:** Fix index method on all controller and add create (fae4438f7a1af7315482fb0accedf3de5e4cdbef)

