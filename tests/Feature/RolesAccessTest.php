<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;

class RolesAccessTest extends TestCase
{
    /** @test */
    public function user_must_login_to_access_to_admin_dashboard(): void
    {
        $this->get(route('board'))
            ->assertRedirect('login');
    }

    /** @test */
    public function admin_can_access_to_admin_dashboard(): void
    {
        //Having
        $adminUser = factory(User::class)->create();

        $adminUser->assignRole('admin');

        $this->actingAs($adminUser);

        //When
        $response = $this->get(route('admin'));

        //Then
        $response->assertOk();
    }

    /** @test */
    public function users_cannot_access_to_admin_dashboard(): void
    {
        //Having
        $user = factory(User::class)->create();

        $user->assignRole('user');

        $this->actingAs($user);

        //When
        $response = $this->get(route('admin'));

        //Then
        $response->assertForbidden();
    }

    /** @test */
    public function user_can_access_to_board(): void
    {
        //Having
        $user = factory(User::class)->create();

        $user->assignRole('user');

        $this->actingAs($user);

        //When
        $response = $this->get('/board/competences');

        //Then
        $response->assertOk();
    }

    /** @test */
    public function admin_cannot_access_to_board(): void
    {
        //Having
        $adminUser = factory(User::class)->create();

        $adminUser->assignRole('admin');

        $this->actingAs($adminUser);

        //When
        $response = $this->get('/board/competences');

        //Then
        $response->assertForbidden();
    }
}
