<?php

/** @var Factory $factory */

use App\Template;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Template::class, function (Faker $faker) {
    return [
        'nom' => 'template-'
    ];
});
