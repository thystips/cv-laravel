<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use App\Competences;
use App\Experiences;
use App\Informations;
use App\Socials;
use Faker\Generator as Faker;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(50),
        'username' => $faker->unique()->userName,
        'custom_cv' => false
    ];
});

$factory->define(Competences::class, function (Faker $faker) {
    return [
        'nom' => $faker->word,
        'description' => $faker->text(20),
        'niveau' => $faker->numberBetween(10,100),
        'type' => $faker->boolean(),
    ];
});

$factory->define(Experiences::class, function (Faker $faker) {
    return [
        'nom' => $faker->word,
        'titre' => $faker->word,
        'date_start' => $faker->date(),
        'date_end' => $faker->date(),
        'description' => $faker->text(10),
    ];
});

$factory->define(Informations::class, function (Faker $faker) {
    return [
        'prenom' => $faker->firstName,
        'nom' => $faker->lastName,
        'adresse' => $faker->address,
        'description' => $faker->text(),
        'telephone' => $faker->phoneNumber,
    ];
});

$factory->define(Socials::class, function (Faker $faker) {
    return [
        'nom' => $faker->word,
        'url' => $faker->url,
        'image' => $faker->imageUrl(),
    ];
});
