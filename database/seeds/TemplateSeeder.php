<?php

use App\Template;
use Illuminate\Database\Seeder;

class TemplateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $items = [
            ['id' => 1, 'nom' => 'template-1'],
            ['id' => 2, 'nom' => 'template-2'],
            ['id' => 3, 'nom' => 'template-3'],
            ['id' => 4, 'nom' => 'template-4'],
            ['id' => 5, 'nom' => 'template-5'],
            ['id' => 6, 'nom' => 'template-6'],
        ];

        foreach ($items as $item) {
            Template::create($item);
        }
    }
}
