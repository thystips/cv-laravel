<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Faker\Generator as Faker;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class UsersTableSeeder extends Seeder implements HasMedia
{
    use HasMediaTrait;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create 10 records of Users
        $users = factory(\App\User::class, 5)->create()->each(function ($user) {

            // Seed the relation with 5 Competences
            $competences = factory(\App\Competences::class, 5)->make();
            $user->Competences()->saveMany($competences);

            // Seed the relation with 5 Experiences
            $experiences = factory(\App\Experiences::class, 5)->make();
            $user->Experiences()->saveMany($experiences);

            // Seed the relation with 1 Information
            $information = factory(\App\Informations::class)->make();
            $user->Informations()->save($information);

            // Seed the relation with 8 Socials
            $socials = factory(\App\Socials::class, 8)->make();
            $user->Socials()->saveMany($socials);

            // Create blank CV
            $user->Informations()->create([]);
            $user->CustomCv()->create([]);

            // Dirctory for customcv
            $dir = 'users/'.$user->username();
            Storage::makeDirectory($dir);

            // Create collection for avatar
            $user->registerMediaCollections();

            // Assign role
            $user->assignRole('user');
        });
    }
}
