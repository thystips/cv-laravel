<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        // Reset cached roles and permissions
        app()['cache']->forget('spatie.permission.cache');

        // Create Permissions
        Permission::create(['name' => 'delete_user']);
        Permission::create(['name' => 'access_admin']);
        Permission::create(['name' => 'access_board']);

        // Create Roles
        Role::create(['name' => 'user'])->givePermissionTo('access_board');
        Role::create(['name' => 'admin'])->givePermissionTo(['delete_user', 'access_admin']);
    }
}
