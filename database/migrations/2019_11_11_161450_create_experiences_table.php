<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExperiencesTable extends Migration
{

	public function up()
	{
		Schema::create('experiences', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->string('nom');
			$table->string('titre');
			$table->date('date_start');
			$table->date('date_end')->nullable();
            $table->string('description')->nullable();
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('experiences');
	}
}
