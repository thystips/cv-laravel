<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInformationsTable extends Migration
{

	public function up()
	{
		Schema::create('informations', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->string('prenom')->nullable();
			$table->string('nom')->nullable();
			$table->string('email')->nullable();
			$table->string('adresse')->nullable();
			$table->text('description')->nullable();
			$table->string('telephone', 22)->nullable();
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('informations');
	}
}
