<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompetencesTable extends Migration
{

	public function up()
	{
		Schema::create('competences', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->string('nom')->nullable();
			$table->string('description')->nullable();
			$table->tinyInteger('niveau')->default('0');
            // true général && false spécifique
            $table->boolean('type')->default(true);
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('competences');
	}
}
