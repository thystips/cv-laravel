$('#navbar span').click(function() {
    $('#navbar span').toggleClass('flip');
    if($(window).width() > 1280) {
        $('#navbar ul').toggleClass('hide');
    } else {
        $('#responsive-menu').toggleClass('reset');
    }
});

// ACCUEIL

let offsetImgSocial = (-(($('#pdp img').offset().top + $('#pdp img').height()) - $('#social').offset().top)) + ($('#social').height() + 40);

$('#unique-skew').css("bottom", offsetImgSocial/1.5);

window.addEventListener('resize', function() {

    let offsetImgSocial = (-(($('#pdp img').offset().top + $('#pdp img').height()) - $('#social').offset().top)) + ($('#social').height() + 40);

    $('#unique-skew').css("bottom", offsetImgSocial/1.5);

});

// INFO SKILLS

let infoSkillsLevelBar = document.querySelectorAll('.level-bar');

infoSkillsLevelBar.forEach(levelBar => {
    
    let width = levelBar.getAttribute('data-width');

    levelBar.style.width = width + "%";

});

// GENERAL SKILLS

let lastRow = document.querySelector('#skills-general').lastElementChild;

if (lastRow.childElementCount == 1) {
    let column = document.createElement('div');
    column.setAttribute('class', 'column');
    lastRow.appendChild(column);
}

// NAVBAR SCROLL

let page = 0;

$(document).ready(function() {
    function scrollTo(number) {
        $('#pages-container').animate({
            scrollLeft:number
        }, 'slow');
    }

    $('#navbar a').click(function(e) {
        e.preventDefault();
    });
    $('#accueil').click(function() {
        page=0;
        scrollTo(0);
    });
    $('#moi').click(function() {
        page=1;
        scrollTo($('.page#accueil-page').outerWidth());
    });
    $('#competences').click(function() {
        page=2;
        scrollTo($('.page#accueil-page').outerWidth()*2);
    });
    $('#contacter').click(function() {
        page=3;
        scrollTo($('.page#accueil-page').outerWidth()*3);
    });
    $('#navbar a').click(function(e) {
        e.preventDefault();
    });
    $('#responsive-accueil').click(function() {
        page=0;
        scrollTo(0);
        $('#responsive-menu').toggleClass('reset');
        $('#navbar span').toggleClass('flip');
    });
    $('#responsive-moi').click(function() {
        page=1;
        scrollTo($('.page#accueil-page').outerWidth());
        $('#responsive-menu').toggleClass('reset');
        $('#navbar span').toggleClass('flip');
    });
    $('#responsive-competences').click(function() {
        page=2;
        scrollTo($('.page#accueil-page').outerWidth()*2);
        $('#responsive-menu').toggleClass('reset');
        $('#navbar span').toggleClass('flip');
    });
    $('#responsive-contacter').click(function() {
        page=3;
        scrollTo($('.page#accueil-page').outerWidth()*3);
        $('#responsive-menu').toggleClass('reset');
        $('#navbar span').toggleClass('flip');
    });


    window.addEventListener('resize', function() {
        if(page == 0) {
            scrollTo(0);
        } else if (page == 1) {
            scrollTo($('.page#accueil-page').outerWidth());
        } else if (page == 2) {
            scrollTo($('.page#accueil-page').outerWidth()*2);
        } else if (page == 3) {
            scrollTo($('.page#accueil-page').outerWidth()*3);
        }

    });

});

$('#contact #submit').click(function() {

    let name = $('#contact #nom').val();
    let email = $('#contact #email').val();
    let message = $('#contact #message').val();
    let name_element = $('#contact #nom');
    let email_element = $('#contact #email');
    let message_element = $('#contact #message');

    if(name == "") {
        name_element.css('border', '1px solid #e74c3c');
    }

    if(email == "") {
        email_element.css('border', '1px solid #e74c3c');
    }

    if(message == "") {
        message_element.css('border', '1px solid #e74c3c');
    }

    if(name != "" && email != "" && message != "") {
        let array = new Array(3);
        array[0] = name;
        array[1] = email;
        array[2] = message;

        $.ajax({
            type: 'POST',
            url: 'mail.php',
            data: {email: array},
            success: function(data) {
            alert('yes');
            console.log(data);
        }})

    }
    
});