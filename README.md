# Projet CVthèque

[![pipeline status](https://gitlab.com/antoine33520/cv-laravel/badges/master/pipeline.svg)](https://gitlab.com/antoine33520/cv-laravel/commits/master)

Documentation: [Larecipe](resources/docs/1.0/index.md)

Site web: [https://cvtheque.antoinethys.com](https://cvtheque.antoinethys.com)
