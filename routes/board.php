<?php

use App\Http\Controllers\AccountController;
use App\Http\Controllers\CompetencesController;
use App\Http\Controllers\CustomCvController;
use App\Http\Controllers\ExperiencesController;
use App\Http\Controllers\InformationsController;
use App\Http\Controllers\SocialController;
use App\Http\Controllers\TemplateController;
use App\Http\Controllers\UserTemplateController;

Route::get('/board', 'AccountController@getUserBoard')->name('board');

// ******ACCOUNT SECTION ROUTES******
Route::resource('/account', 'AccountController')->except([
    'edit'
]);
Route::get('/account/{id}/edit', function ($id) {
    $user = new AccountController();
    return $user->edit($id);
})->name('editaccount');

// ******COMPETENCES SECTION ROUTES******
Route::resource('/board/competences', 'CompetencesController')->except([
    'edit'
]);
Route::get('/board/competences/{id}/edit', function ($id) {
    $comp = new CompetencesController();
    return $comp->edit($id);
})->name('editcomp');

// ******INFORMATIONS SECTION ROUTES******
Route::resource('/board/informations', 'InformationsController')->except([
    'edit'
]);
Route::get('/board/informations/{id}/edit', function ($id) {
    $info = new InformationsController();
    return $info->edit($id);
})->name('editinfo');

// ******EXPERIENCES SECTION ROUTES******
Route::resource('/board/experiences', 'ExperiencesController')->except([
    'edit'
]);
Route::get('/board/experiences/{id}/edit', function ($id) {
    $exp = new ExperiencesController();
    return $exp->edit($id);
})->name('editexp');

// ******SOCIAL SECTION ROUTES******
Route::resource('/board/socials', 'SocialController')->except([
    'edit'
]);
Route::get('/board/socials/{id}/edit', function ($id) {
    $social = new SocialController();
    return $social->edit($id);
})->name('editsocial');

// *******CUSTOM CV SECTION ROUTES*******
Route::resource('/board/customcv', 'CustomCvController')->except([
    'edit'
]);
Route::get('/board/customcv/{id}/edit', function ($id) {
    $customcv = new CustomCvController();
    return $customcv->edit($id);
})->name('editcustomcv');
Route::get('/board/customcv/download/{id}', 'CustomCvController@download')->name('dlcustomcv');

// ******USER TEMPLATE SECTION ROUTES******
Route::resource('/board/template', 'UserTemplateController')->only([
    'index',
    'edit',
    'update'
]);
