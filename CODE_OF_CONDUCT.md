# Code of conduct for this project / Code de conduite pour ce projet

## Languages

- [English/Anglais](docs/CODE_OF_CONDUCT_EN.md)
- [French/Français](docs/CODE_OF_CONDUCT_FR.md)
