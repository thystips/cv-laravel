// ******************************************
// *********** HEADER RESPONSIVE ************
// ******************************************

let menuHamburger = document.getElementById("menu-nav-mobile-toggle");
let menuSpans = menuHamburger.children[0].children;
let hamburgerOpen = false;
let menuNavMobile = document.getElementById("menu-nav-mobile");

let menuOpen = () => {
    hamburgerOpen = !hamburgerOpen;
    menuSpans[1].style.cssText = "opacity: 0; transform: scale(0.8);";
    menuSpans[0].style.cssText = "transform: rotateZ(45deg);";
    menuSpans[2].style.cssText = "transform: rotateZ(-45deg);";
    menuNavMobile.style.cssText = "transform: translateY(0%); top: 40px;";
    document.body.style.cssText = "overflow: hidden;";
};

let menuClose = () => {
    hamburgerOpen = !hamburgerOpen;
    menuSpans[1].style.cssText = "";
    menuSpans[0].style.cssText = "";
    menuSpans[2].style.cssText = "";
    menuNavMobile.style.cssText = "transform: translateY(-100%); ";
    setTimeout(() => {
        document.body.style.cssText = "overflow: none;";
    }, 100);
};

menuHamburger.addEventListener("click", () => {
    if (!hamburgerOpen) {
        menuOpen();
    } else {
        menuClose();
    }
});

// CLOSE MENU IF RESPONSIVE IS OFF

window.addEventListener("resize", () => {
    if (window.innerWidth > 1024 && hamburgerOpen) {
        menuClose();
    }
});
