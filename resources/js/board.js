let now = document.getElementById("now");
let nowToggle = document.getElementById("now-toggle");

now.addEventListener("change", () => {
    nowToggleStyle = getComputedStyle(nowToggle);
    if (nowToggleStyle.getPropertyValue("display") == "flex") {
        nowToggle.style.display = "none";
    } else {
        nowToggle.style.display = "flex";
    }
});
