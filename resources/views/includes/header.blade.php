<header class="menu-main">

    <!-- NAVBAR BRAND LOGO -->

    <div class="menu-logo">
        <a href="{{ route('index') }}"><img src="{{ asset('images/logo.png') }}" alt="Ynov Campus"></a>
    </div>

    <!-- MAIN NAVBAR NO RESPONSIVE -->

    <div class="menu-nav">
        <ul>
            <div class="menu-global">
                <li><a href="#">Accueil</a></li>
                <li><a href="#">Modèles</a></li>
                <li><a href="#">Créer mon cv</a></li>
            </div>
            <div class="menu-user-section">

                @if (Route::has('login'))
                @auth

                <li>
                    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="btn-primary">Déconnexion</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
                </li>
                <li>
                    <a href="{{ url('/board/competences') }}" class="btn-secondary">Mon compte</a>
                </li>

                @else
                @if (Route::has('register'))
                <li>
                    <a href="{{ route('register') }}" class="btn-secondary">S'inscrire</a>
                </li>
                @endif

                <li>
                    <a href="{{ route('login') }}" class="btn-primary">Se connecter</a>
                </li>

                @endif
                @endauth
            </div>
        </ul>
    </div>

    <!-- HAMBURGER TOGGLE RESPONSIVE MENU -->

    <div id="menu-nav-mobile-toggle">
        <div class="hamburger">
            <span class="top"></span>
            <span class="mid"></span>
            <span class="bot"></span>
        </div>
    </div>

</header>

<!-- RESPONSIVE NAVBAR MENU SECTION -->

<div class="menu-nav-mobile" id="menu-nav-mobile">
    <ul>
        <div class="menu-global-mobile">
            <li><a href="#">Accueil</a></li>
            <li><a href="#">Modèles</a></li>
            <li><a href="#">Créer mon cv</a></li>
        </div>
        <div class="menu-user-section-mobile">

            @if (Route::has('login'))
            @auth

            <li>
                <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="btn-primary">Déconnexion</a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
            </li>
            <li>
                <a href="{{ url('/board/competences') }}" class="btn-secondary">Mon compte</a>
            </li>

            @else
            @if (Route::has('register'))
            <li>
                <a href="{{ route('register') }}" class="btn-secondary">S'inscrire</a>
            </li>
            @endif

            <li>
                <a href="{{ route('login') }}" class="btn-primary">Se connecter</a>
            </li>

            @endif
            @endauth
        </div>
    </ul>
</div>