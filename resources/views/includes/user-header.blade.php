<header class="menu-main">

    <!-- NAVBAR BRAND LOGO -->

    <div class="menu-logo">
        <a href="{{ route('index') }}"><img src="{{ asset('images/logo.png') }}" alt="Ynov Campus"></a>
    </div>

    <!-- MAIN NAVBAR NO RESPONSIVE -->

    <div class="menu-nav">
        <ul>
            <div class="menu-global">
                <li><a href="{{ route('index') }}">Accueil</a></li>
                <li><a href="{{ route('account.index') }}">Mes informations</a></li>
                <li><a href="{{ route('competences.index') }}">Tableau de bord</a></li>
            </div>
            <div class="menu-user-section">

                <li>
                    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="btn-primary">Déconnexion</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
                </li>
                <li>
                    <a href="{{ route('customcv.show', Auth::id()) }}" class="btn-secondary">Voir mon CV</a>
                </li>

            </div>
        </ul>
    </div>

    <!-- HAMBURGER TOGGLE RESPONSIVE MENU -->

    <div id="menu-nav-mobile-toggle">
        <div class="hamburger">
            <span class="top"></span>
            <span class="mid"></span>
            <span class="bot"></span>
        </div>
    </div>

</header>

<!-- RESPONSIVE NAVBAR MENU SECTION -->

<div class="menu-nav-mobile" id="menu-nav-mobile">
    <ul>
        <div class="menu-global-mobile">
            <li><a href="{{ route('index') }}">Accueil</a></li>
            <li><a href="{{ route('account.index') }}">Mes informations</a></li>
            <li><a href="{{ route('competences.index') }}">Tableau de bord</a></li>
        </div>
        <div class="menu-user-section-mobile">
            <li>
                <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="btn-primary">Déconnexion</a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
            </li>
            <li>
                <a href="{{ route('customcv.show', Auth::id()) }}" class="btn-secondary">Voir mon CV</a>
            </li>
        </div>
    </ul>
</div>