<div class="board-header">
    <ul class="menu-global">

        @if(Auth::user()->custom_cv == 0)

        @if($page == 'competences')
        <li><a href="" class="active">Compétences</a></li>
        @else
        <li><a href="{{ action('CompetencesController@index') }}">Compétences</a></li>
        @endif

        @if($page == 'experiences')
        <li><a href="" class="active">Expériences</a></li>
        @else
        <li><a href="{{ action('ExperiencesController@index') }}">Expériences</a></li>
        @endif

        @if($page == 'informations')
        <li><a href="" class="active">Profil</a></li>
        @else
        <li><a href="{{ action('InformationsController@index') }}">Profil</a></li>
        @endif

        @if($page == 'social')
        <li><a href="" class="active">Liens sociaux</a></li>
        @else
        <li><a href="{{ action('SocialController@index') }}">Liens sociaux</a></li>
        @endif

        @if($page == 'template')
        <li><a href="" class="active">Template</a></li>
        @else
        <li><a href="{{ route('template.index') }}">Template</a></li>
        @endif

        @if($page == 'customcv')
        <li><a href="" class="active">CV Personnalisé</a></li>
        @else
        <li><a href="{{ action('CustomCvController@index') }}">CV Personnalisé</a></li>
        @endif

        @else

        <li><a href="#" class="inactive">Compétences</a></li>
        <li><a href="#" class="inactive">Expériences</a></li>
        <li><a href="#" class="inactive">Profil</a></li>
        <li><a href="#" class="inactive">Liens sociaux</a></li>
        <li><a href="#" class="inactive">Template</a></li>
        <li><a href="#" class="inactive">CV Personnalisé</a></li>

        @endif

    </ul>


    @if($page == 'competences')
    <a class="btn-primary" href="{{ action('CompetencesController@create') }}">
        Ajouter une compétence
    </a>
    @elseif($page == 'experiences')
    <a class="btn-primary" href="{{ action('ExperiencesController@create') }}">
        Ajouter une expériences
    </a>
    @elseif($page == 'social')
    <a class="btn-primary" href="{{ action('SocialController@create') }}">
        Ajouter une réseau social
    </a>
    @else
    @endif

</div>