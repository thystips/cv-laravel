<!DOCTYPE html>
<html lang="fr">

<head>
    <title>@yield('title') | CV</title>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media 			queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/			html5shiv.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/			respond.min.js"></script>
        <![endif]-->

    @if (parse_url(('/'), PHP_URL_SCHEME) == "HTTPS")
    <link href="{{ asset_secure('css/style.css') }}" rel="stylesheet">
    @else
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    @endif

</head>

<body>

    @yield('body')

    @if (parse_url(('/'), PHP_URL_SCHEME) == "HTTPS")
    <script src="{{ asset_secure('js/index.js') }}" rel="stylesheet"></script>
    @else
    <script src="{{ asset('js/index.js') }}" rel="stylesheet"></script>
    @endif

</body>

</html>