{!! Form::open(array('route' => 'route.name', 'method' => 'POST')) !!}
	<ul>
		<li>
			{!! Form::label('nom', 'Nom:') !!}
			{!! Form::text('nom') !!}
		</li>
		<li>
			{!! Form::label('titre', 'Titre:') !!}
			{!! Form::text('titre') !!}
		</li>
		<li>
			{!! Form::label('date_start', 'Date_start:') !!}
			{!! Form::text('date_start') !!}
		</li>
		<li>
			{!! Form::label('date_end', 'Date_end:') !!}
			{!! Form::text('date_end') !!}
		</li>
		<li>
			{!! Form::label('description', 'Description:') !!}
			{!! Form::text('description') !!}
		</li>
		<li>
			{!! Form::submit() !!}
		</li>
	</ul>
{!! Form::close() !!}