@extends('layouts.page-template')

@section('title')
Informations
@endsection

@section('body')

@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

@include('includes.user-header')

<div class="container-full">
    <div class="board">

        <div class="board-title">
            Informations du compte
        </div>

        <div class="board-content">

            @if(isset($user))

            <div class="item">
                <div class="item-title">
                    Intitulé
                </div>
                <div class="item-content">
                    <div class="row">
                        <label for="name" class="item-label">
                            Nom complet :
                        </label>
                        <input name="name" type="text" class="inputbox" value="{{ $user->name }}" autocomplete="off" disabled>
                    </div>
                </div>
            </div>

            <div class="item">
                <div class="item-title">
                    Nom de compte
                </div>
                <div class="item-content">
                    <div class="row">
                        <label for="username" class="item-label">
                            Pseudonyme :
                        </label>
                        <input name="username" type="text" class="inputbox" value="{{ $user->username }}" autocomplete="off" disabled>
                    </div>
                </div>
            </div>

            <div class="item">
                <div class="item-title">
                    Contact
                </div>
                <div class="item-content">
                    <div class="row">
                        <label for="email" class="item-label">
                            Email :
                        </label>
                        <input name="email" type="text" class="inputbox" value="{{ $user->email }}" autocomplete="off" disabled>
                    </div>
                </div>
            </div>

            <div class="item">
                <div class="item-title">
                    Custom CV
                </div>
                <div class="item-content">
                    <div class="row">
                        <input name="custom_cv" type="checkbox" autocomplete="off" value="1" disabled @if($user->custom_cv == 1) checked @endif>
                        <label for="custom_cv" class="disabled item-label center checkbox">
                            Uploader mon propre CV
                        </label>
                    </div>
                </div>
            </div>

            <div class="row">
                <a class="btn-primary" href="{{ route('editaccount', ['id' => Auth::id()]) }}">Modifier</a>
            </div>

            @else
            <div>'user' not found</div>
            @endif

        </div>

    </div>
</div>

@endsection