@extends('layouts.page-template')

@section('title')
Tableau de bord
@endsection

@section('body')

@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

@if (isset($success))
<div>{{ $success }}</div>
@endif

@include('includes.user-header')

<div class="container-full">
    <div class="board">

        <!-- TITLE SECTION BOARD -->
        <div class="board-title">
            Modifier le template du CV
        </div>

        <!-- CV CONTENT SECTION BOARD -->
        <div class="board-content">

            <!-- COMPETENCES SECTION BOARD CONTENT -->

            @if(isset($temps))
            <form action="{{ route('template.update', Auth::id()) }}" method="post">
                @csrf
                @method('PUT')

                <div class="item">
                    <div class="item-title">
                        Template
                    </div>

                    <div class="item-content">
                        <div class="row">
                            <label for="id" class="item-label">Choisissez votre Template</label>
                            <select name="id" id="id" class="inputbox">
                                @foreach($temps as $temp)
                                <option value="{{ $temp->id }}">{{ $temp->nom }}</option>
                                @endforeach
                            </select>
                        </div>

                    </div>


                </div>
                <div class="row">
                    <input type="submit" value="Enregistrer" class="btn-primary">
                    <form action="{{ route('template.index') }}" method="get">
                        <input class="btn-warning" type="submit" value="Retour">
                    </form>
                </div>

            </form>

            @else
            <div>die: 'temps' not found</div>
            @endif
        </div>

    </div>

</div>

@endsection