@extends('layouts.page-template')

@section('title')
Tableau de bord
@endsection

@section('body')

@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<?php $page = "customcv" ?>

@include('includes.user-header')

<div class="container-full">
    <div class="board">

        <!-- TITLE SECTION BOARD -->
        <div class="board-title">
            Modifier les informations du CV
        </div>

        <div class="board-content">

            <form action="{{ route('customcv.update', Auth::id()) }}" method="post" enctype="multipart/form-data">

                @csrf
                @method('PUT')

                <div class="item">
                    <div class="item-title">
                        Importer mon CV
                    </div>

                    <div class="item-content">

                        <div class="row">
                            <label for="customcv" class="item-label center">
                                Mon CV en PDF :
                            </label>
                            <input type="file" name="customcv" id="customcv" class="inputfile">
                        </div>

                    </div>
                </div>
                <div class="row">
                    <input type="submit" class="btn-primary" value="Enregistrer">
                    <a href="{{ route('customcv.index') }}" class="btn-warning">
                        Retour
                    </a>
                </div>

            </form>

        </div>

    </div>
</div>

@endsection