@extends('layouts.page-template')

@section('title')
Tableau de bord
@endsection

@section('body')

<?php $page = "customcv" ?>

@include('includes.user-header')

<div class="container-full">
    <div class="board">

        <!-- TITLE SECTION BOARD -->
        <div class="board-title">
            Tableau de bord
        </div>

        <!-- HEADER SECTION BOARD -->
        @include('includes.board-header')

        <div class="board-content">

            <div class="item">
                <div class="item-title">
                    Importer mon CV
                </div>

                <div class="item-content">

                    <div class="row">
                        <label for="customcv" class="item-label">
                            Mon CV en PDF :
                        </label>
                        @if($customCv->link === $path)
                        <input type="text" name="customcv" id="customcv" value="Votre CV personnalisé est en ligne" class="inputbox" disabled>
                        @elseif(Auth::user()->custom_cv === 0)
                        <input type="text" name="customcv" id="customcv" value="Vous devez activer l'upload de CV personnalisé dans les paramètres du compte !" class="inputbox" disabled>
                        @elseif($customCv->link !== $path && $customCv->link !== NULL)
                        <input type="text" name="customcv" id="customcv" value="Il semble y avoir une erreur, veuillez réenvoyer votre CV" class="inputbox" disabled>
                        @else
                        <input type="text" name="customcv" id="customcv" value="Votre n'avez pas encore ajouter votre CV" class="inputbox" disabled>
                        @endif
                    </div>
                    <div class="row">
                        @if(Auth::user()->custom_cv === 1)
                        <a href="{{ route('editcustomcv', Auth::id()) }}" class="btn-primary">Modifier</a>
                        @elseif(Auth::user()->custom_cv === 0)
                        <a href="#" class="btn-warning">Modifier</a>
                        @endif
                        <a href="{{ route('dlcustomcv', Auth::id()) }}" class="btn-primary">Télécharger</a>
                        <a href="{{ route('showcustomcv', Auth::id()) }}" class="btn-secondary">Voir mon CV</a>
                    </div>
                </div>
            </div>


        </div>

    </div>
</div>

@endsection