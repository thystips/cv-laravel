@extends('layouts.page-template')

@section('title')
Tableau de bord
@endsection

@section('body')

<?php $page = "informations"; ?>

@include('includes.user-header')

@if(isset($success))
<div>{{ $success }}</div>
@endif
<div class="container-full">
    <div class="board">

        <!-- TITLE SECTION BOARD -->
        <div class="board-title">
            Tableau de bord
        </div>

        <!-- HEADER SECTION BOARD -->
        @include('includes.board-header')

        <!-- CV CONTENT SECTION BOARD -->
        <div class="board-content">

            @if(isset($infos) && isset($avatar))

            <!-- EXPERIENCES SECTION BOARD CONTENT -->

            <div class="item">
                <div class="item-title">Photo de profil :</div>
                <div class="item-content">
                    <div class="row">
                        <label for="avatar" class="item-label center">
                            @if(isset($avatar[0]))
                            <img src="{{ $avatar[0]->getUrl() }}" alt="" width="150">
                            @else
                            <p>Pas d'avatar</p>
                            @endif
                        </label>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="item-title">Intitulé :</div>
                <div class="item-content">
                    <div class="row">
                        <label for="nom" class="item-label">Nom</label>
                        <input name="nom" id="nom" type="text" class="inputbox" value="{{ $infos->nom }}" autocomplete="off" disabled>
                    </div>
                    <div class="row">
                        <label for="prenom" class="item-label">Prénom</label>
                        <input name="prenom" id="prenom" type="text" class="inputbox" value="{{ $infos->prenom }}" autocomplete="off" disabled>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="item-title">Localisation :</div>
                <div class="item-content">
                    <div class="row">
                        <label for="adresse" class="item-label">Adresse :</label>
                        <input class="inputbox" type="text" name="adresse" id="adresse" value="{{ $infos->adresse }}" autocomplete="off" disabled>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="item-title">Contact :</div>
                <div class="item-content">
                    <div class="row">
                        <label for="telephone" class="item-label">Téléphone :</label>
                        <input class="inputbox" type="text" name="telephone" id="telephone" value="{{ $infos->telephone }}" autocomplete="off" disabled>
                    </div>
                    <div class="row">
                        <label for="email" class="item-label">Email :</label>
                        <input class="inputbox" type="text" name="email" id="email" value="{{ $infos->email }}" autocomplete="off" disabled>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="item-title">Description de soi :</div>
                <div class="item-content">
                    <div class="row">
                        <label for="description" class="item-label">Qui suis-je :</label>
                        <textarea class="inputbox" type="text" name="description" id="description" autocomplete="off" disabled rows="15">{{ $infos->description }}</textarea>
                    </div>
                </div>
            </div>

            <div class="row">
                <a class="btn-primary" href="{{ route('editinfo', Auth::id()) }}">Modifier</a>
            </div>

            @else
            <div>die: 'comps' not found</div>
            @endif

        </div>
    </div>
</div>

@endsection