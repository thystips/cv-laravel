@extends('layouts.page-template')

@section('title')
Tableau de bord
@endsection

@section('body')

@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

@include('includes.user-header')

<div class="container-full">
    <div class="board">

        <!-- TITLE SECTION BOARD -->
        <div class="board-title">
            Modifier les informations du CV
        </div>

        <div class="board-content">

            @if(isset($comp))

            <form action="{{ action('CompetencesController@update', $comp->id) }}" method="post">

                @csrf
                @method('PUT')

                <!-- EXPERIENCES SECTION BOARD CONTENT -->

                <div class="item">
                    <div class="item-title">
                        Compétence
                    </div>
                    <div class="item-content">

                        <div class="row">
                            <label for="nom" class="item-label">
                                Nom :
                            </label>
                            <input name="nom" type="text" class="inputbox" value="{{ $comp->nom }}" autocomplete="off">
                        </div>
                        <div class="row">
                            <label for="description" class="item-label">
                                Description :
                            </label>
                            <input name="description" type="text" class="inputbox" value="{{ $comp->description }}" autocomplete="off">
                        </div>
                        <div class="row">
                            <label for="niveau" class="item-label">
                                Niveau :
                            </label>
                            <input name="niveau" type="text" class="inputbox" value="{{ $comp->niveau }}" autocomplete="off">
                        </div>
                        <div class="row">
                            <input type="submit" class="btn-primary" value="Modifier">
                            <form action="{{ route('competences.index') }}" method="get">
                                <input class="btn-warning" type="submit" value="Retour">
                            </form>
                        </div>

                    </div>
                </div>
            </form>
            @else
            <div>not found</div>
            @endif

        </div>
    </div>
</div>

@endsection