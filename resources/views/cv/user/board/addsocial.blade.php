@extends('layouts.page-template')

@section('title')
Tableau de bord
@endsection

@section('body')

@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

@include('includes.user-header')

<div class="container-full">
    <div class="board">

        <div class="board-title">
            Ajouter un lien social
        </div>

        <div class="board-content addform">

            <form action="{{ action('SocialController@store') }}" method="post">

                {{ csrf_field() }}

                <div class="item">
                    <div class="item-title">
                        Lien Social
                    </div>
                    <div class="item-content">

                        <div class="row">
                            <label for="nom" class="item-label">
                                Nom :
                            </label>
                            <input name="nom" type="text" class="inputbox">
                        </div>
                        <div class="row">
                            <label for="url" class="item-label">
                                Lien :
                            </label>
                            <input name="url" type="text" class="inputbox">

                        </div>
                        <div class="row">
                            <label for="image" class="item-label">
                                Image :
                            </label>
                            <input name="image" type="text" class="inputbox">
                        </div>

                        <div class="row">
                            <input type="submit" class="btn-primary" value="Enregistrer">
                            <a href="{{ route('socials.index') }}" class="btn-warning">Retour</a>
                        </div>
                    </div>
                </div>

            </form>

        </div>

    </div>
</div>

@endsection