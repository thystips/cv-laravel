@extends('layouts.page-template')

@section('title')
Tableau de bord
@endsection

@section('body')

@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

@if (isset($success))
<div>{{ $success }}</div>
@endif

@include('includes.user-header')

<div class="container-full">
    <div class="board">

        <!-- TITLE SECTION BOARD -->
        <div class="board-title">
            Modifier les informations du CV
        </div>

        <div class="board-content">

            @if(isset($infos))

            <form action="{{ action('InformationsController@update', $infos->id) }}" method="post" enctype="multipart/form-data">

                @csrf

                <!-- EXPERIENCES SECTION BOARD CONTENT -->

                <div class="item">
                    <div class="item-title">Photo de profil :</div>
                    <div class="item-content">
                        <div class="row">
                            <label for="avatar" class="item-label center">Image de profil</label>
                            <input type="file" name="avatar" id="avatar">
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="item-title">Intitulé :</div>
                    <div class="item-content">
                        <div class="row">
                            <label for="nom" class="item-label">Nom</label>
                            <input name="nom" id="nom" type="text" class="inputbox" value="{{ $infos->nom }}" autocomplete="off">
                        </div>
                        <div class="row">
                            <label for="prenom" class="item-label">Prénom</label>
                            <input name="prenom" id="prenom" type="text" class="inputbox" value="{{ $infos->prenom }}" autocomplete="off">
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="item-title">Localisation :</div>
                    <div class="item-content">
                        <div class="row">
                            <label for="adresse" class="item-label">Adresse :</label>
                            <input class="inputbox" type="text" name="adresse" id="adresse" value="{{ $infos->adresse }}" autocomplete="off">
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="item-title">Contact :</div>
                    <div class="item-content">
                        <div class="row">
                            <label for="telephone" class="item-label">Téléphone :</label>
                            <input class="inputbox" type="text" name="telephone" id="telephone" value="{{ $infos->telephone }}" autocomplete="off">
                        </div>
                        <div class="row">
                            <label for="email" class="item-label">Email :</label>
                            <input class="inputbox" type="text" name="email" id="email" value="{{ $infos->email }}" autocomplete="off">
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="item-title">Description de soi :</div>
                    <div class="item-content">
                        <div class="row">
                            <label for="description" class="item-label">Qui suis-je :</label>
                            <textarea class="inputbox" type="text" name="description" id="description" rows="15">{{ $infos->description }}</textarea>
                        </div>
                    </div>
                </div>

                {{ method_field('PUT') }}
                <div class="row">
                    <input type="submit" class="btn-primary" value="Enregistrer">
                    <form action="{{ route('informations.index') }}" method="get">
                        <input class="btn-warning" type="submit" value="Retour">
                    </form>
                </div>

            </form>

            @else
            <div>die: 'infos' not found</div>
            @endif

        </div>
    </div>
</div>

@endsection