@extends('layouts.page-template')

@section('title')
Nouvelle Compétence
@endsection

@section('body')

@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

@include('includes.header')
<div class="container-full">
    <div class="board">
        <!-- TITLE SECTION BORD -->
        <div class="board-title">Ajouter une compétence</div>

        <!-- CONTENT SECTION BOARD -->
        <div class="board-content addform">
            <form action="{{ action('CompetencesController@store') }}" method="post">
                {{ csrf_field() }}
                <div class="item">
                    <div class="item-title">Nouvelle Compétence</div>
                    <div class="item-content">
                        <div class="row">
                            <label for="nom" class="item-label">
                                Nom :
                            </label>
                            <input name="nom" type="text" class="inputbox" autocomplete="off">
                        </div>
                        <div class="row">
                            <label for="description" class="item-label">
                                Description :
                            </label>
                            <input name="description" type="text" class="inputbox" autocomplete="off">
                        </div>
                        <div class="row">
                            <label for="niveau" class="item-label">
                                Niveau :
                            </label>
                            <input name="niveau" type="text" class="inputbox" autocomplete="off">
                        </div>
                        <div class="row">
                            <input type="submit" class="btn-primary" value="Enregistrer">
                            <a href="{{ route('competences.index') }}" class="btn-warning">Retour</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>

    </div>
</div>

@endsection