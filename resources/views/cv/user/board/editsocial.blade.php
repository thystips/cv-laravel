@extends('layouts.page-template')

@section('title')
Tableau de bord
@endsection

@section('body')

@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

@include('includes.user-header')

<div class="container-full">
    <div class="board">

        <div class="board-title">
            Modifier les informations du CV
        </div>

        <div class="board-content">

            @if(isset($social))

            <form action="{{ action('SocialController@update', $social->id) }}" method="post">

                @csrf
                @method('PUT')

                <div class="item">
                    <div class="item-title">
                        Lien Social
                    </div>
                    <div class="item-content">

                        <div class="row">
                            <label for="nom" class="item-label">
                                Nom :
                            </label>
                            <input name="nom" type="text" class="inputbox" autocomplete="off" value="{{ $social->nom }}">
                        </div>
                        <div class="row">
                            <label for="url" class="item-label">
                                Lien :
                            </label>
                            <input name="url" type="text" class="inputbox" autocomplete="off" value="{{ $social->url }}">
                        </div>
                        <div class="row">
                            <label for="image" class="item-label">
                                Image :
                            </label>
                            <input name="image" type="text" class="inputbox" autocomplete="off" value="{{ $social->image }}">
                        </div>
                        <div class="row">
                            <input type="submit" class="btn-primary" value="Modifier">
                            <form action="{{ route('socials.index') }}" method="get">
                                <input class="btn-warning" type="submit" value="Retour">
                            </form>
                        </div>
                    </div>
                </div>

            </form>

            @endif

        </div>

    </div>
</div>

@endsection