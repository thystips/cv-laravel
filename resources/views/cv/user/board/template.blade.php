@extends('layouts.page-template')

@section('title')
Tableau de bord
@endsection

@section('body')

<?php $page = "template" ?>

@include('includes.user-header')

<div class="container-full">
    <div class="board">

        <!-- TITLE SECTION BOARD -->
        <div class="board-title">
            Tableau de bord
        </div>

        <!-- HEADER SECTION BOARD -->
        @include('includes.board-header')

        <!-- CV CONTENT SECTION BOARD -->
        <div class="board-content">

            <!-- COMPETENCES SECTION BOARD CONTENT -->

            @if(isset($temp))

            <div class="item">
                <div class="item-title">
                    Template
                </div>

                <div class="item-content">
                    <div class="row">
                        <label for="nom" class="item-label">Votre template actuel</label>
                        <input type="text" name="nom" id="nom" class="inputbox" disabled autocomplete="off" value="template-{{ $temp }}">
                    </div>

                </div>


            </div>
            <div class="row">
                <a href="{{ route('template.edit', Auth::id()) }}" class="btn-primary">Modifier</a>
            </div>

            @else
            <div>die: 'temps' not found</div>
            @endif
        </div>

    </div>

</div>


@endsection