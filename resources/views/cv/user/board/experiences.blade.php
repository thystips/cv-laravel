@extends('layouts.page-template')

@section('title')
Tableau de bord
@endsection

@section('body')
<?php $page = "experiences" ?>

@include('includes.user-header')

<div class="container-full">
    <div class="board">

        <!-- TITLE SECTION BOARD -->
        <div class="board-title">
            Tableau de bord
        </div>

        <!-- HEADER SECTION BOARD -->
        @include('includes.board-header')

        <!-- CV CONTENT SECTION BOARD -->
        <div class="board-content">

            <!-- EXPERIENCES SECTION BOARD CONTENT -->

            @if(isset($exps))
            <?php $id = 1; ?>
            @foreach ($exps as $exp)

            <div class="item">
                <div class="item-title">
                    Expérience {{ $id }}
                </div>
                <div class="item-content">

                    <div class="row">
                        <label for="name" class="item-label">
                            Nom :
                        </label>
                        <input name="name" type="text" class="inputbox" value="{{ $exp->nom }}" autocomplete="off" disabled>
                    </div>
                    <div class="row">
                        <label for="name" class="item-label">
                            Titre :
                        </label>
                        <input name="name" type="text" class="inputbox" value="{{ $exp->titre }}" autocomplete="off" disabled>
                    </div>
                    <div class="row">
                        <label for="date_start" class="item-label">
                            Date de début :
                        </label>
                        <input name="date_start" type="date" class="inputbox" value="{{ $exp->date_start }}" autocomplete="off" disabled>
                    </div>
                    <div class="row">
                        <label for="date_end" class="item-label">
                            Date de fin :
                        </label>
                        <input name="date_end" type="date" class="inputbox" value="{{ $exp->date_end }}" autocomplete="off" disabled>
                    </div>
                    <div class="row">
                        <label for="description" class="item-label">
                            Description :
                        </label>
                        <textarea class="inputbox" name="description" id="description" cols="30" rows="10" disabled>{{ $exp->description }}</textarea>
                    </div>
                    <div class="row">
                        <a href="{{ route('editexp', ['id' => $exp->id]) }}" class="btn-primary">Modifier</a>
                        <form action="{{ action('ExperiencesController@destroy', $exp->id) }}" method="post">
                            @csrf
                            @method('DELETE')
                            <input class="btn-warning" type="submit" value="Supprimer">
                        </form>
                    </div>
                </div>
            </div>
            <?php $id++; ?>
            @endforeach
            @else
            <div>die: 'comps' not found</div>
            @endif
        </div>
    </div>
</div>

@endsection