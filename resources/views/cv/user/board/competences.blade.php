@extends('layouts.page-template')

@section('title')
Tableau de bord
@endsection

@section('body')

<?php $page = "competences" ?>

@include('includes.user-header')

<div class="container-full">
    <div class="board">

        <!-- TITLE SECTION BOARD -->
        <div class="board-title">
            Tableau de bord
        </div>

        <!-- HEADER SECTION BOARD -->
        @include('includes.board-header')

        <!-- CV CONTENT SECTION BOARD -->
        <div class="board-content">

            <!-- COMPETENCES SECTION BOARD CONTENT -->

            @if(isset($comps))
            <?php $id = 1; ?>
            @foreach ($comps as $comp)

            <div class="item">
                <div class="item-title">
                    Compétence {{ $id }}
                </div>
                <div class="item-content">

                    <div class="row">
                        <label for="name" class="item-label">
                            Nom :
                        </label>
                        <input name="name" type="text" class="inputbox" value="{{ $comp->nom }}" autocomplete="off" disabled>
                    </div>
                    <div class="row">
                        <label for="description" class="item-label">
                            Description :
                        </label>
                        <input name="description" type="text" class="inputbox" value="{{ $comp->description }}" autocomplete="off" disabled>

                    </div>
                    <div class="row">
                        <label for="level" class="item-label">
                            Niveau :
                        </label>
                        <input name="level" type="text" class="inputbox" value="{{ $comp->niveau }}" autocomplete="off" disabled>
                    </div>
                    <div class="row">
                        <a href="{{ route('editcomp', ['id' => $comp->id]) }}" class="btn-primary">Modifier</a>
                        <form action="{{ action('CompetencesController@destroy', $comp->id) }}" method="post">
                            @csrf
                            @method('DELETE')
                            <input class="btn-warning" type="submit" value="Supprimer">
                        </form>
                    </div>

                </div>
            </div>
            <?php $id++; ?>
            @endforeach
            @else
            <div>die: 'comps' not found</div>
            @endif
        </div>

    </div>

</div>

<script src="{{ asset('js/board.js')"></script>

@endsection