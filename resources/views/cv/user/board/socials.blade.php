@extends('layouts.page-template')

@section('title')
Tableau de bord
@endsection

@section('body')

<?php $page = "social" ?>

@include('includes.user-header')

@if(isset($success))
<div>{{ $success }}</div>
@endif
<div class="container-full">
    <div class="board">

        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        <!-- TITLE SECTION BOARD -->
        <div class="board-title">
            Tableau de bord
        </div>

        <!-- HEADER SECTION BOARD -->
        @include('includes.board-header')

        <!-- CV CONTENT SECTION BOARD -->
        <div class="board-content">

            @if(isset($socials))
            <?php $id = 1; ?>
            @foreach ($socials as $social)

            <div class="item">
                <div class="item-title">
                    Lien Social
                </div>
                <div class="item-content">

                    <div class="row">
                        <label for="nom" class="item-label">
                            Nom :
                        </label>
                        <input name="nom" type="text" class="inputbox" value="{{ $social->nom }}" disabled>
                    </div>
                    <div class="row">
                        <label for="url" class="item-label">
                            Lien :
                        </label>
                        <input name="url" type="text" class="inputbox" value="{{ $social->url }}" disabled>

                    </div>
                    <div class="row">
                        <label for="image" class="item-label">
                            Image :
                        </label>
                        <input name="image" type="text" class="inputbox" value="{{ $social->image }}" disabled>
                    </div>
                    <div class="row">
                        <a href="{{ route('editsocial', ['id' => $social->id]) }}" class="btn-primary">Modifier</a>
                        <form action="{{ action('SocialController@destroy', $social->id) }}" method="post">
                            @csrf
                            @method('DELETE')
                            <input class="btn-warning" type="submit" value="Supprimer">
                        </form>
                    </div>
                </div>
            </div>
            <?php $id++; ?>
            @endforeach
            @else
            <div>die: 'comps' not found</div>
            @endif

        </div>

    </div>

</div>

@endsection