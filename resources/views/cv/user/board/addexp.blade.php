@extends('layouts.page-template')

@section('title')
Nouvelle Expérience
@endsection

@section('body')

@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

@include('includes.header')
<div class="board">
    <!-- TITLE SECTION BORD -->
    <div class="board-title">Ajouter une Expérience</div>

    <!-- CONTENT SECTION BOARD -->
    <div class="board-content addform">
        <form action="{{ action('ExperiencesController@store') }}" method="post">
            {{ csrf_field() }}
            <div class="item">
                <div class="item-title">Nouvelle Expérience</div>
                <div class="item-content">
                    <div class="row">
                        <label for="nom" class="item-label">
                            Nom :
                        </label>
                        <input name="nom" type="text" class="inputbox" autocomplete="off">
                    </div>
                    <div class="row">
                        <label for="titre" class="item-label">
                            Titre :
                        </label>
                        <input name="titre" type="text" class="inputbox" autocomplete="off">
                    </div>
                    <div class="row">
                        <label for="date_start" class="item-label">
                            Date de début :
                        </label>
                        <input name="date_start" type="date" class="inputbox" autocomplete="off">
                    </div>
                    <div class="row center">
                        <input type="checkbox" name="now" id="now">
                        <label for="now" class="item-label checkbox">En cours</label>
                    </div>
                    <div class="row" id="now-toggle">
                        <label for="date_end" class="item-label">
                            Date de fin :
                        </label>
                        <input name="date_end" type="date" class="inputbox" autocomplete="off">
                    </div>
                    <div class="row">
                        <label for="description" class="item-label">
                            Description :
                        </label>
                        <input name="description" type="text" class="inputbox" autocomplete="off">
                    </div>
                    <div class="row">
                        <input type="submit" class="btn-primary" value="Enregistrer">
                        <a href="{{ route('experiences.index') }}" class="btn-warning">Retour</a>
                    </div>
                </div>
            </div>
        </form>
    </div>

</div>

<script src="{{ asset('js/board.js') }}"></script>

@endsection