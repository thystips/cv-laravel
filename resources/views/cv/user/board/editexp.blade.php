@extends('layouts.page-template')

@section('title')
Tableau de bord
@endsection

@section('body')

@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

@include('includes.user-header')

<div class="container-full">
    <div class="board">

        <!-- TITLE SECTION BOARD -->
        <div class="board-title">
            Modifier les informations du CV
        </div>

        <div class="board-content">

            @if(isset($exp))

            <form action="{{ action('ExperiencesController@update', $exp->id) }}" method="post">

                @csrf
                @method('PUT')

                <div class="item">
                    <div class="item-title">
                        Expérience
                    </div>
                    <div class="item-content">

                        <div class="row">
                            <label for="nom" class="item-label">
                                Nom :
                            </label>
                            <input name="nom" type="text" class="inputbox" value="{{ $exp->nom }}" autocomplete="off">
                        </div>
                        <div class="row">
                            <label for="titre" class="item-label">
                                Titre :
                            </label>
                            <input name="titre" type="text" class="inputbox" value="{{ $exp->titre }}" autocomplete="off">
                        </div>
                        <div class="row">
                            <label for="date_start" class="item-label">
                                Date de début :
                            </label>
                            <input name="date_start" type="date" class="inputbox" value="{{ $exp->date_start }}" autocomplete="off">
                        </div>
                        <div class="row center">
                            <input type="checkbox" name="now" id="now" onchange="toggle('now-toggle')">
                            <label for="now" class="item-label checkbox">En cours</label>
                        </div>
                        <div class="row" id="now-toggle">
                            <label for="date_end" class="item-label">
                                Date de fin :
                            </label>
                            <input name="date_end" type="date" class="inputbox" value="{{ $exp->date_end }}" autocomplete="off">
                        </div>
                        <div class="row">
                            <label for="description" class="item-label">
                                Description :
                            </label>
                            <textarea class="inputbox" name="description" id="description" cols="30" rows="10">{{ $exp->description }}</textarea>
                        </div>
                        <div class="row">
                            <input type="submit" class="btn-primary" value="Enregistrer">
                            <form action="{{ route('experiences.index') }}" method="get">
                                <input class="btn-warning" type="submit" value="Retour">
                            </form>
                        </div>
                    </div>
                </div>
            </form>
            @else
            <div>die: 'comps' not found</div>
            @endif

        </div>
    </div>
</div>

<script src="{{ asset('js/board.js') }}"></script>

@endsection