<!DOCTYPE html>
<html lang="fr">

<head>
    <title>@yield('title') | CV</title>

    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media 			queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/			html5shiv.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/			respond.min.js"></script>
        <![endif]-->

    @if (parse_url(('/'), PHP_URL_SCHEME) == "HTTPS")
    <link href="{{ asset_secure('storage/template-1/css/style.css') }}" rel="stylesheet" />
    <link href="{{ asset_secure('storage/template-1/css/mobile.style.css') }}" rel="stylesheet" />
    @else
    <link href="{{ asset('storage/template-1/css/style.css') }}" rel="stylesheet" />
    <link href="{{ asset('storage/template-1/css/mobile.style.css') }}" rel="stylesheet" />
    @endif
</head>

<body>
    @yield('var')

    <nav id="navbar">
        <span><img id="close" src="{{ asset('storage/template-1/src/menu_cross.svg') }}" alt="" /><img id="open" src="{{ asset('storage/template-1/src/menu.svg') }}" alt="" /></span>
        <ul class="hide">
            <li><a id="accueil" href="#">ACCUEIL</a></li>
            <li><a id="moi" href="#">MOI</a></li>
            <li><a id="competences" href="#">MES COMPÉTENCES</a></li>
            <li><a id="contacter" href="#">ME CONTACTER</a></li>
        </ul>
    </nav>

    <div id="responsive-menu" class="full">
        <div id="responsive-menu-container">
            <ul>
                <li><a id="responsive-accueil" href="#">ACCUEIL</a></li>
                <li><a id="responsive-moi" href="#">MOI</a></li>
                <li>
                    <a id="responsive-competences" href="#">MES COMPÉTENCES</a>
                </li>
                <li>
                    <a id="responsive-contacter" href="#">ME CONTACTER</a>
                </li>
            </ul>
        </div>
    </div>

    <div id="pages-container">
        <!-- PAGE ACCUEIL -->

        <div class="page" id="accueil-page">
            <div class="full">
                <div id="unique-skew" class="full">
                    <div class="image"></div>
                </div>
            </div>

            <div id="pdp">
                <h1>
                    {{ $cv->informations->prenom }}
                    {{ $cv->informations->nom }}
                </h1>
                <h3>
                    ETUDIANT
                </h3>
                @if(isset($avatar[0]))
                <img src="{{ $avatar[0]->getUrl() }}" alt="Louis Lacoste" />
                @else
                <img src="{{ asset('/storage/template-1/src/no_image.png') }}" alt="Louis Lacoste" />
                @endif
            </div>

            <ul id="social">
                @foreach($cv->socials as $social)

                <li>
                    <a href="{{ $social->url }}" target="_blank">
                        <img src="{{ $social->image }}" alt="{{ $social->nom }}" />
                    </a>
                </li>

                @endforeach
            </ul>
        </div>

        <!-- PAGE MOI -->

        <div class="page" id="moi-page">
            <div class="table-container">
                <!-- <div class="table-item" id="informations">
                        <h1 class="table-item-title">informations</h1>

                        <ul>
                            <li>première info</li>
                            <li>seconde info</li>
                        </ul>
                    </div> -->
                <div class="table-item" id="formations">
                    <h1 class="table-item-title">formations</h1>

                    <table>
                        @foreach($cv->experiences as $exp)

                        <tr class="formation-content">
                            <td>
                                {{ date_format(date_create($exp->date_start), 'Y') }}
                            </td>
                            <td class="formation-text">
                                {{$exp->nom}} - {{$exp->titre}} <br />
                                {{$exp->description}}
                            </td>
                        </tr>
                        <tr class="formation-line">
                            <td>
                                <span></span>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                </div>
                <div class="table-item" id="bio">
                    <h1 class="table-item-title">qui suis-je</h1>

                    <div>
                        {{ $cv->informations->description }}
                    </div>
                </div>
            </div>
        </div>

        <!-- PAGE COMPETENCES -->

        <div class="page" id="skills">
            <div class="skills-container">
                <div class="skills-item">
                    <div class="skills-title">
                        compétences spéciales
                    </div>

                    <div id="skills-info">
                        @foreach($cv->competences as $comp)
                        <div class="row">
                            <div class="skills-info-header">
                                <div class="skills-info-title">
                                    {{ $comp->nom }}
                                </div>
                                <div class="skills-info-level">
                                    <div class="level-bar" data-width="{{ $comp->niveau }}">
                                        {{ $comp->niveau }} %
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>

                <div class="skills-item">
                    <div class="skills-title">compétences générales</div>

                    <div id="skills-general">
                        <div class="row">
                            <div class="column">
                                <div class="skills-general-header">
                                    <div class="skills-general-title">
                                        ANGLAIS
                                    </div>

                                    <div class="skills-general-level">
                                        80 %
                                    </div>
                                </div>

                                <div class="skills-general-header">
                                    <div class="skills-general-title">
                                        ANGLAIS
                                    </div>

                                    <div class="skills-general-level">
                                        80 %
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- PAGE CONTACT -->

        <div class="page" id="contact">
            <div id="contact-form-container">
                <form id="form">
                    <input type="text" name="nom" id="nom" placeholder="votre nom" />

                    <input type="email" name="email" id="email" placeholder="votre e-mail" />
                </form>

                <textarea name="message" id="message" placeholder="vore message"></textarea>

                <br />

                <button id="submit">envoyer le message</button>
            </div>
        </div>
    </div>

    <script src="{{
                asset('storage/template-1/script/jquery.min.js')
            }}"></script>
    <script src="{{
                asset('storage/template-1/script/script.js')
            }}"></script>
</body>

</html>