@extends('layouts.page-template')

@section('title')
Connexion
@endsection

@section('body')

@include('includes.header')
<div class="form-background"></div>
<div class="content">
    <div class="form">
        <form class="form-primary" action="{{ route('login') }}" method="post">

            <!-- TITLE FORM -->

            <h1 class="form-title">
                Se Connecter
            </h1>

            <!-- ERRORS SECTION FORM -->
            @if ($errors->has('username') || $errors->has('email'))
            <span class="form-alert" role="alert">
                {{ $errors->first('username') ?: $errors->first('email') }}
            </span>
            @enderror
            @error('password')
            <span class="form-alert" role="alert">
                {{ $message }}
            </span>
            @enderror

            @csrf
            <!-- EMAIL SECTION FORM LOGIN -->
            <label for="text">{{ __('EMAIL OU MOT DE PASSE') }}</label>
            <input type="text" name="text" id="text" class="inputbox {{ $errors->has('username') || $errors->has('email') ? 'invalid-input' : '' }}" value="{{ old('email') }}" required autocomplete="text" autofocus>

            <!-- PASSWORD SECTION FORM LOGIN  -->
            <label for="password">{{ __('MOT DE PASSE') }}</label>
            <input type="password" name="password" id="password" class="inputbox  @error('password') invalid-input @enderror" required autocomplete="current-password">

            <!-- FORGOT PASSWORD GESTION LINK -->
            @if (Route::has('password.request'))
            <a class="link" href="{{ route('password.request') }}">
                {{ __('Mot de passe oublié ?') }}
            </a>
            @endif


            <span class="remember">

                <input class="" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                <label for="remember">
                    {{ __('RESTER CONNECTÉ') }}
                </label>

            </span>

            <input class="btn-primary" type="submit" value="{{ __('SE CONNECTER') }}">
        </form>
        <div class="form-secondary">

            <h2 class="form-title">
                {{ __('Pas encore de compte ?') }}
            </h2>

            <div class="bottom">
                <!-- <button class="btn-primary">
                    SE CONNECTER
                </button> -->
                <a class="btn-secondary" href="{{ route('register') }}">
                    S'INSCRIRE
                </a>
            </div>

            <div class="darken-background"></div>
        </div>
    </div>
</div>
@endsection