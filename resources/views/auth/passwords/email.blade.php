@extends('layouts.page-template')

@section('title')
Mot de passe oublié
@endsection

@section('body')

@include('includes.header')

<div class="form-background"></div>
<div class="content">
    <div class="form">
        <form method="post" action="{{ route('password.email') }}" class="form-primary">

            @csrf

            <!-- HOME BUTTON -->
            <span class="home">
                <a href="{{ route('index') }}">
                    <img src="{{ asset('images/home.svg') }}" alt="">
                </a>
            </span>

            <!-- TITLE FORM -->
            <h1 class="form-title">
                {{ __('changer de mot de passe')}}
            </h1>

            <!-- ERROS SECTION FORM -->
            @error('email')
            <span class="form-alert" role="alert">
                {{ $message }}
            </span>
            @enderror

            @if (session('status'))
            <div role="alert">
                {{ __('L\'EMAIL A ETE ENVOYE') }}
            </div>
            @endif

            <!-- EMAIL SECTION UPDATE PASSWORD -->
            <label for="email">{{ __('ADRESSE EMAIL') }}</label>
            <input id="email" type="email" class="inputbox @error('email') invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

            <!-- SUBMIT RESET PASSWORD -->
            <input class="btn-primary" type="submit" value="{{ __('ENVOYER') }}">

        </form>
        <div class="form-secondary">
            <h2 class="form-secondary-title">
                {{ __('Mauvaise page ?') }}
            </h2>

            <div class="bottom">
                <a href="{{ route('login') }}" class="btn-primary">
                    {{ __('SE CONNECTER') }}
                </a>
                <a href="{{ route('register') }}" class="btn-secondary">
                    {{ __('S\'INSCRIRE') }}
                </a>
            </div>
        </div>
    </div>
</div>

@endsection