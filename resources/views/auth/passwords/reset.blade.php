@extends('layouts.page-template')

@section('title')
Nouveau mot de passe
@endsection

@section('body')

@include('includes.header')

<div class="form-background"></div>
<div class="content">
    <div class="form">
        <form action="{{ route('password.update') }}" class="form-primary" method="post">

            <!-- TITLE FORM -->

            <h1 class="form-title">
                {{ __('nouveau mot de passe') }}
            </h1>

            <!-- ERRORS SECTION FORM -->
            @error('email')
            <span class="form-alert" role="alert">
                {{ $message }}
            </span>
            @enderror
            @error('password')
            <span class="form-alert" role="alert">
                {{ $message }}
            </span>
            @enderror

            @csrf
            <input type="hidden" name="token" value="{{ $token }}">

            <!-- EMAIL SECTION UPDATE PASSWORD FORM -->

            <label for="email">{{ __('ADRESSE EMAIL') }}</label>
            <input id="email" type="email" class="inputbox @error('email') invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>

            <!-- ********** UPDATE PASSWORD CLICKED LINK SECTION ********* -->
            <!-- PASSWORD SECTION UPDATE PASSWORD FORM -->

            <label for="password">{{ __('MOT DE PASSE') }}</label>
            <input id="password" type="password" class="inputbox @error('password') invalid @enderror" name="password" required autocomplete="new-password">

            <!-- CONFIRM PASSWORD SECTION UPDATE PASSWORD FORM -->
            <label for="password-confirm">{{ __('CONFIRMER LE MOT DE PASSE') }}</label>
            <input id="password-confirm" type="password" class="inputbox" name="password_confirmation" required autocomplete="new-password">

            <!-- SUBMIT UPDATE PASSWORD -->
            <input class="btn-primary" type="submit" value="{{ __('METTRE A JOUR') }}">

        </form>
        <div class="form-secondary">
            <h2 class="form-title">
                {{ __('Mauvaise page ?') }}
            </h2>

            <div class="bottom">
                <a href="{{ route('login') }}" class="btn-primary">
                    {{ __('SE CONNECTER') }}
                </a>
                <a href="{{ route('register') }}" class="btn-secondary">
                    {{ __('S\'INSCRIRE') }}
                </a>
            </div>
        </div>
    </div>
</div>

@endsection