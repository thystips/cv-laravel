@extends('layouts.page-template')

@section('title')
Inscription
@endsection

@section('body')

@include('includes.header')
<div class="form-background"></div>
<div class="content">
    <div class="form">
        <form class="form-primary" action="{{ route('register') }}" method="post">

            <!-- TITLE FORM -->

            <h1 class="form-title">
                S'inscrire
            </h1>

            <!-- ERRORS SECTION FORM -->

            @error('name')
            <span class="form-alert" role="alert">
                {{ $message }}
            </span>
            @enderror
            @error('username')
            <span class="form-alert" role="alert">
                {{ $message }}
            </span>
            @enderror
            @error('email')
            <span class="form-alert" role="alert">
                {{ $message }}
            </span>
            @enderror
            @error('password')
            <span class="form-alert" role="alert">
                {{ $message }}
            </span>
            @enderror

            @csrf


            <!-- NAME SECTION REGISTER FORM -->

            <label for="name">{{ __('NOM') }}</label>
            <input id="name" type="text" class="inputbox @error('name') invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="off" autofocus>

            <!-- USERNAME SECTION FORM -->

            <label for="username">{{ __('NOM D\'UTILISATEUR') }}</label>
            <input id="username" type="text" class="inputbox @error('username') invalid @enderror" name="username" value="{{ old('username') }}" required autocomplete="username" autofocus>

            <!-- EMAIL SECTION REGISTER FORM -->

            <label for="email">{{ __('ADRESSE EMAIL') }}</label>
            <input id="email" type="email" class="inputbox @error('email') invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="off">

            <!-- PASSWORD SECTION REGISTER FORM -->

            <label for="password">{{ __('MOT DE PASSE') }}</label>
            <input id="password" type="password" class="inputbox @error('password') invalid @enderror" name="password" required autocomplete="off">

            <!-- CONFIRM PASSWORD SECTION REGISTER FORM -->
            <label for="password-confirm">{{ __('CONFIRMER MOT DE PASSE') }}</label>
            <input id="password-confirm" type="password" class="inputbox @error('password') invalid @enderror" name="password_confirmation" required autocomplete="off">

            <input class="btn-primary" type="submit" value="{{ __('S\'INSCRIRE') }}">

        </form>
        <div class="form-secondary">
            <h2 class="form-title">
                {{ __('Déjà un inscrit ?') }}
            </h2>

            <div class="bottom">
                <!-- <button class="btn-primary">
                    SE CONNECTER
                </button> -->
                <a class="btn-secondary" href="{{ route('login') }}">
                    SE CONNECTER
                </a>
            </div>

            <div class="darken-background"></div>
        </div>
    </div>
</div>
@endsection