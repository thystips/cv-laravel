@extends('layouts.page-template') @section('title') Verify @endsection
@section('body')

@include('includes.header')

<div class="form-background"></div>
<div class="content">
    <div class="form">
        <form class="form-primary" method="POST" action="{{ route('verification.resend') }}">

            <!-- TITLE FORM -->

            <h1 class="form-title">
                Vérification Email
            </h1>

            <!-- ERRORS FORM SECTION -->

            @if (session('resent'))
            <div class="form-alert" role="alert">
                {{ __("Un nouvel email vous à été envoyé !") }}
            </div>
            @endif

            <label>
                Avant de continuer, veillez regarder votre boîte mail pour
                vôtre lien d'activation du compte
            </label>
            <label>
                Si vous ne trouvez pas l'Email de vérification de compte :
            </label>
            @csrf
            <input type="submit" class="btn-primary" value="ENVOYER UN AUTRE EMAIL" align="center" />
        </form>
    </div>
</div>


@endsection