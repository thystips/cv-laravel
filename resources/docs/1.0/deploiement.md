# Déploiement du site

---

- [Déploiement du site](#dploiement-du-site)
  - [Création du fichier `.env`](#cration-du-fichier-env)
  - [Récupération du code](#rcupration-du-code)
  - [Installation des paquets composer](#installation-des-paquets-composer)
  - [Installation des paquets npm](#installation-des-paquets-npm)
  - [Déploiement de la BDD](#dploiement-de-la-bdd)
  - [Création d'utilisateurs de test](#cration-dutilisateurs-de-test)
  
<a name="env"></a>

## Création du fichier `.env`

Pour créer le fichier `.env` il suffit de copier le contenu du fichier `.env.production` et d'en modifier les données nécessaires comme la connexion à votre base de données.
_Pour développement il suffit de remplacer `production` par `local` et mettre `APP_DEBUG` à `true`._

<a name="code"></a>

## Récupération du code

- En ssh :
`git clone git@gitlab.com:antoine33520/cv-laravel.git`\
ou
- En https:
`git clone https://gitlab.com/antoine33520/cv-laravel.git`

<a name="composer"></a>

## Installation des paquets composer
 
 ```bash
$ composer install
Loading composer repositories with package information
Installing dependencies (including require-dev) from lock file
Package operations: 133 installs, 0 updates, 0 removals
...
> Illuminate\Foundation\ComposerScripts::postAutoloadDump
> @php artisan package:discover --ansi
Discovered Package: barryvdh/laravel-ide-helper
Discovered Package: binarytorch/larecipe
Discovered Package: facade/ignition
Discovered Package: fideloper/proxy
Discovered Package: intervention/image
Discovered Package: jorijn/laravel-security-checker
Discovered Package: laravel/tinker
Discovered Package: laravel/ui
Discovered Package: nesbot/carbon
Discovered Package: nunomaduro/collision
Discovered Package: propaganistas/laravel-phone
Discovered Package: salmanzafar/laravel-repository-pattern
Discovered Package: spatie/laravel-medialibrary
Discovered Package: spatie/laravel-permission
Package manifest generated successfully.
````
<a name="npm"></a>

## Installation des paquets npm

```bash
$ npm install

> undefined postinstall /home/antoinethys/dev/cv-laravel
> npm run prod


> @ prod /repo/cv-laravel
> npm run production


> @ production /repo/cv-laravel
> cross-env NODE_ENV=production node_modules/webpack/bin/webpack.js --no-progress --hide-modules --config=node_modules/laravel-mix/setup/webpack.config.js



 DONE  Compiled successfully in 2055ms                                                                                                                                                 10:43:12

         Asset      Size  Chunks             Chunk Names
/css/style.css    12 KiB       0  [emitted]  /js/board
  /js/board.js  1.21 KiB       0  [emitted]  /js/board
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fsevents@1.2.9 (node_modules/fsevents):
npm WARN notsup SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@1.2.9: wanted {"os":"darwin","arch":"any"} (current: {"os":"linux","arch":"x64"})

audited 17196 packages in 8.789s

3 packages are looking for funding
  run `npm fund` for details
```

<a name="npm-dev"></a>

### Pour le développement (uniquement)

Pour obtenir un css et js plus facilement:

```bash
$ npm run dev

> @ dev /repo/cv-laravel
> npm run development


> @ development /repo/cv-laravel
> cross-env NODE_ENV=development node_modules/webpack/bin/webpack.js --progress --hide-modules --config=node_modules/laravel-mix/setup/webpack.config.js

98% after emitting SizeLimitsPlugin

 DONE  Compiled successfully in 1165ms                                                                                                                                                 11:29:21

         Asset      Size     Chunks             Chunk Names
/css/style.css  14.8 KiB  /js/board  [emitted]  /js/board
  /js/board.js   4.9 KiB  /js/board  [emitted]  /js/board
```

<a name="npm-prod"></a>

### Pour la production

Pour regénérer le js et css au format adapté à la mise en production

```bash
$ npm run prod

> @ prod /repo/cv-laravel
> npm run production


> @ production /repo/cv-laravel
> cross-env NODE_ENV=production node_modules/webpack/bin/webpack.js --no-progress --hide-modules --config=node_modules/laravel-mix/setup/webpack.config.js



 DONE  Compiled successfully in 1341ms                                                                                                                                                 11:31:07

         Asset      Size  Chunks             Chunk Names
/css/style.css    12 KiB       0  [emitted]  /js/board
  /js/board.js  1.21 KiB       0  [emitted]  /js/board
```

<a name="bdd"></a>

## Déploiement de la BDD

Pour mettre en place la Base de Données nécessaire au fonctionnement de l'application:

```bash
$ php artisan migrate --seed
Migration table created successfully.
Migrating: 2014_10_12_000000_create_users_table
Migrated:  2014_10_12_000000_create_users_table (0.12 seconds)
Migrating: 2014_10_12_100000_create_password_resets_table
Migrated:  2014_10_12_100000_create_password_resets_table (0.12 seconds)
Migrating: 2019_08_19_000000_create_failed_jobs_table
Migrated:  2019_08_19_000000_create_failed_jobs_table (0.09 seconds)
Migrating: 2019_11_11_161450_create_competences_table
Migrated:  2019_11_11_161450_create_competences_table (0.29 seconds)
Migrating: 2019_11_11_161450_create_experiences_table
Migrated:  2019_11_11_161450_create_experiences_table (0.25 seconds)
Migrating: 2019_11_11_161450_create_informations_table
Migrated:  2019_11_11_161450_create_informations_table (0.31 seconds)
Migrating: 2019_11_11_161450_create_socials_table
Migrated:  2019_11_11_161450_create_socials_table (0.28 seconds)
Migrating: 2019_11_21_083448_add_username_to_user_table
Migrated:  2019_11_21_083448_add_username_to_user_table (0.08 seconds)
Migrating: 2019_11_28_183426_add_custom_cv_to_users_table
Migrated:  2019_11_28_183426_add_custom_cv_to_users_table (0.03 seconds)
Migrating: 2019_11_28_211542_create_custom_cvs_table
Migrated:  2019_11_28_211542_create_custom_cvs_table (0.28 seconds)
Migrating: 2019_11_29_181542_create_permission_tables
Migrated:  2019_11_29_181542_create_permission_tables (1.98 seconds)
Migrating: 2019_12_02_165058_create_template_table
Migrated:  2019_12_02_165058_create_template_table (0.07 seconds)
Migrating: 2019_12_02_172222_add_template_to_users_table
Migrated:  2019_12_02_172222_add_template_to_users_table (0.02 seconds)
Migrating: 2019_12_03_124415_create_media_table
Migrated:  2019_12_03_124415_create_media_table (0.13 seconds)
Seeding: RolesAndPermissionsSeeder
Seeded:  RolesAndPermissionsSeeder (0.14 seconds)
Seeding: AdminSeeder
Seeded:  AdminSeeder (0.03 seconds)
Seeding: TemplateSeeder
Seeded:  TemplateSeeder (0.06 seconds)
Database seeding completed successfully.
```

Cette action va également effectuer un `seeding` pour créer la liste des templates, les rôles ainsi qu'un administrateur dont le mot de passe par défaut est `password` et l'adresse email `admin@ynov.com`.

<a name="user-seeding"></a>

## Création d'utilisateurs de test **_(facultatif)_**

Afin de créer 5 utilisateurs il suffit d'effectuer un seeding.

```bash
$ php artisan db:seed --class=UsersTableSeeder
Database seeding completed successfully.
```
