# Projet

---

- [Projet](#projet)
  - [Authentification](#authentification)
    - [Installation](#installation)
    - [La factory](#la-factory)
    - [Utilisateurs](#utilisateurs)
  - [BDD](#bdd)
  - [Stockage](#stockage)
  - [PHPCS](#phpcs)
  - [security-checker](#security-checker)
  - [Repo](#repo)

<a name="authentification"></a>

## Authentification

Authentification en utilisant `Eloquent` :

<a name="installation"></a>

### Installation

```bash
$ composer require laravel/ui --dev
Using version ^1.1 for laravel/ui
./composer.json has been updated
[...]

$ npm install && npm run dev
[...]
       Asset      Size   Chunks             Chunk Names
/css/app.css   191 KiB  /js/app  [emitted]  /js/app
  /js/app.js  1.38 MiB  /js/app  [emitted]  /js/app

$ php artisan ui:auth
[...]
Authentication scaffolding generated successfully.

$ php artisan migrate
Migration table created successfully.
Migrating: 2014_10_12_000000_create_users_table
Migrated:  2014_10_12_000000_create_users_table (0.11 seconds)
Migrating: 2014_10_12_100000_create_password_resets_table
Migrated:  2014_10_12_100000_create_password_resets_table (0.14 seconds)
Migrating: 2019_08_19_000000_create_failed_jobs_table
Migrated:  2019_08_19_000000_create_failed_jobs_table (0.06 seconds)
```

<a name="la-factory"></a>

### La factory

Si non automatiquement crée :

```bash
$ php artisan make:factory UserFactory
[...]
```

Ceci sera utiliser par la suite pour la créer d'utilisateurs à la volée.

<a name="utilisateurs"></a>

### Utilisateurs

Pour générer des utilisateurs dans la base de données il suffit de créer un `seeder` :

```bash
$ php artisan make:seeder UserTableSeeder
Seeder created successfully.
```

Une fois le fichier `/database/seeds/UserTableSeeder.php` créé il faut le modifier afin de créer plusieurs utilisateurs (10 dans cet exemple).

```php
<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class, 10)->create();
    }
}

```

Il faut ensuite modifier le fichier `/database/seeds/DatabaseSeeder.php` pour ajouter notre nouveau `seeder`.

```php
<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
    }
}
```

Pour éviter les erreurs :

```bash
$ composer dumpautoload
Generating optimized autoload files> Illuminate\Foundation\ComposerScripts::postAutoloadDump
> @php artisan package:discover --ansi
Discovered Package: binarytorch/larecipe
Discovered Package: facade/ignition
Discovered Package: fideloper/proxy
Discovered Package: laravel/tinker
Discovered Package: laravel/ui
Discovered Package: nesbot/carbon
Discovered Package: nunomaduro/collision
Package manifest generated successfully.
Generated optimized autoload files containing 3876 classes
```

Le lancement des `seeders` peut se faire de plusieurs façons :

- Tous les seeders :

```bash
$ php artisan db:seed
Seeding: UsersTableSeeder
Seeded:  UsersTableSeeder (0.07 seconds)
Database seeding completed successfully.
```

- Un seul :

```bash
$ php artisan db:seed --class=UsersTableSeeder
Database seeding completed successfully.
```

- Avec la regénération de la BDD :

```bash
$ php artisan migrate:fresh --seed
Dropped all tables successfully.
Migration table created successfully.
Migrating: 2014_10_12_000000_create_users_table
Migrated:  2014_10_12_000000_create_users_table (0.11 seconds)
Migrating: 2014_10_12_100000_create_password_resets_table
Migrated:  2014_10_12_100000_create_password_resets_table (0.1 seconds)
Migrating: 2019_08_19_000000_create_failed_jobs_table
Migrated:  2019_08_19_000000_create_failed_jobs_table (0.04 seconds)
Seeding: UsersTableSeeder
Seeded:  UsersTableSeeder (0.05 seconds)
Database seeding completed successfully.
```

<a name="bdd"></a>

## BDD

[MCD](http://www.laravelsd.com/share/uknZ01)

```bash
$ php artisan make:model -a Informations
Model created successfully.
Factory created successfully.
Created Migration: 2019_11_11_155604_create_informations_table
Controller created successfully.


$ php artisan make:model -a Competences
Model created successfully.
Factory created successfully.
Created Migration: 2019_11_11_155612_create_competences_table
Controller created successfully.

$ php artisan make:model -a Experiences
Model created successfully.
Factory created successfully.
Created Migration: 2019_11_11_155625_create_experiences_table
Controller created successfully.

$ php artisan make:model -a Social
Model created successfully.
Factory created successfully.
Created Migration: 2019_11_11_155634_create_socials_table
Controller created successfully.

$ php artisan make:migration add_is_admin_to_users_table --table=users
Created Migration: 2019_11_11_174305_add_is_admin_to_users_table
```

<a name="stockage"></a>

## Stockage

Afin de rendre accessible `storage/app/public` via `public/storage` il suffit d'une seule commande.

```bash
$ php artisan storage:link
The [public/storage] directory has been linked.
```

<a name="phpcs"></a>

## PHPCS

`PHP CodeSniffer` permet d'effectuer des vérification du code php.

```bash
$ composer require --dev squizlabs/php_codesniffer
Using version ^3.5 for squizlabs/php_codesniffer
./composer.json has been updated
Loading composer repositories with package information
Updating dependencies (including require-dev)
Package operations: 1 install, 0 updates, 0 removals
  - Installing squizlabs/php_codesniffer (3.5.2): Downloading (100%)
Writing lock file
Generating optimized autoload files
> Illuminate\Foundation\ComposerScripts::postAutoloadDump
> @php artisan package:discover --ansi
Discovered Package: binarytorch/larecipe
Discovered Package: facade/ignition
Discovered Package: fideloper/proxy
Discovered Package: laravel/tinker
Discovered Package: laravel/ui
Discovered Package: nesbot/carbon
Discovered Package: nunomaduro/collision
Package manifest generated successfully.
```

Sa configuration se fait via un fichier `phpcs.xml` à la racine du projet, pour ceci un peu d'aide [ici](https://github.com/squizlabs/PHP_CodeSniffer/wiki/Customisable-Sniff-Properties) et [ici](https://medium.com/@nandosalles/the-ruleset-phpcs-for-my-laravel-projects-a54cb3c95b31).

Ensuite pour effectuer les test il suffit d'une seule commande.

```bash
$ ./vendor/bin/phpcs
EE.EE...... 11 / 11 (100%)



FILE: /home/antoinethys/dev/cv-laravel/app/Socials.php
----------------------------------------------------------------------------------------------------------------------------------------------
FOUND 1 ERROR AFFECTING 1 LINE
----------------------------------------------------------------------------------------------------------------------------------------------
 19 | ERROR | [x] The closing brace for the class must go on the next line after the body
    |       |     (PSR2.Classes.ClassDeclaration.CloseBraceAfterBody)
----------------------------------------------------------------------------------------------------------------------------------------------
PHPCBF CAN FIX THE 1 MARKED SNIFF VIOLATIONS AUTOMATICALLY
----------------------------------------------------------------------------------------------------------------------------------------------


FILE: /home/antoinethys/dev/cv-laravel/app/Competences.php
----------------------------------------------------------------------------------------------------------------------------------------------
FOUND 1 ERROR AFFECTING 1 LINE
----------------------------------------------------------------------------------------------------------------------------------------------
 19 | ERROR | [x] The closing brace for the class must go on the next line after the body
    |       |     (PSR2.Classes.ClassDeclaration.CloseBraceAfterBody)
----------------------------------------------------------------------------------------------------------------------------------------------
PHPCBF CAN FIX THE 1 MARKED SNIFF VIOLATIONS AUTOMATICALLY
----------------------------------------------------------------------------------------------------------------------------------------------


FILE: /home/antoinethys/dev/cv-laravel/app/Informations.php
----------------------------------------------------------------------------------------------------------------------------------------------
FOUND 1 ERROR AFFECTING 1 LINE
----------------------------------------------------------------------------------------------------------------------------------------------
 19 | ERROR | [x] The closing brace for the class must go on the next line after the body
    |       |     (PSR2.Classes.ClassDeclaration.CloseBraceAfterBody)
----------------------------------------------------------------------------------------------------------------------------------------------
PHPCBF CAN FIX THE 1 MARKED SNIFF VIOLATIONS AUTOMATICALLY
----------------------------------------------------------------------------------------------------------------------------------------------


FILE: /home/antoinethys/dev/cv-laravel/app/Experiences.php
----------------------------------------------------------------------------------------------------------------------------------------------
FOUND 1 ERROR AFFECTING 1 LINE
----------------------------------------------------------------------------------------------------------------------------------------------
 19 | ERROR | [x] The closing brace for the class must go on the next line after the body
    |       |     (PSR2.Classes.ClassDeclaration.CloseBraceAfterBody)
----------------------------------------------------------------------------------------------------------------------------------------------
PHPCBF CAN FIX THE 1 MARKED SNIFF VIOLATIONS AUTOMATICALLY
----------------------------------------------------------------------------------------------------------------------------------------------

Time: 52ms; Memory: 6MB
```

Ensuite `phpcbf` permet de faire une partie des corrections automatiquement. Comme toutes corrections automatique il est important de faire attention aux modifications effectuées.

```bash
$ ./vendor/bin/phpcbf
FF.FF...... 11 / 11 (100%)



PHPCBF RESULT SUMMARY
--------------------------------------------------------------------------
FILE                                                      FIXED  REMAINING
--------------------------------------------------------------------------
/home/antoinethys/dev/cv-laravel/app/Socials.php          1      0
/home/antoinethys/dev/cv-laravel/app/Competences.php      1      0
/home/antoinethys/dev/cv-laravel/app/Informations.php     1      0
/home/antoinethys/dev/cv-laravel/app/Experiences.php      1      0
--------------------------------------------------------------------------
A TOTAL OF 4 ERRORS WERE FIXED IN 4 FILES
--------------------------------------------------------------------------

Time: 159ms; Memory: 8MB
```

On vérifie ensuite si des erreurs subsistent.

```bash
$ ./vendor/bin/phpcs
........... 11 / 11 (100%)


Time: 48ms; Memory: 8MB
```

<a name="security-checker"></a>

## security-checker

Sensiolabs met à disposition un vérificateur de failles de sécurité.

```bash
$ composer require jorijn/laravel-security-checker --dev
Using version ^1.1 for jorijn/laravel-security-checker
./composer.json has been updated
Loading composer repositories with package information
Updating dependencies (including require-dev)
Package operations: 8 installs, 0 updates, 0 removals
  - Installing ralouphie/getallheaders (3.0.3): Loading from cache
  - Installing psr/http-message (1.0.1): Loading from cache
  - Installing guzzlehttp/psr7 (1.6.1): Loading from cache
  - Installing guzzlehttp/promises (v1.3.1): Loading from cache
  - Installing guzzlehttp/guzzle (6.4.1): Loading from cache
  - Installing composer/ca-bundle (1.2.4): Downloading (100%)
  - Installing sensiolabs/security-checker (v5.0.3): Downloading (100%)
  - Installing jorijn/laravel-security-checker (v1.1.1): Downloading (100%)
guzzlehttp/psr7 suggests installing zendframework/zend-httphandlerrunner (Emit PSR-7 responses)
jorijn/laravel-security-checker suggests installing laravel/slack-notification-channel (In Laravel 5.8 they extracted the Slack notification channel into a first-party package. If you run 5.8, you will need to install this package too.)
Writing lock file
Generating optimized autoload files
> Illuminate\Foundation\ComposerScripts::postAutoloadDump
> @php artisan package:discover --ansi
Discovered Package: binarytorch/larecipe
Discovered Package: facade/ignition
Discovered Package: fideloper/proxy
Discovered Package: jorijn/laravel-security-checker
Discovered Package: laravel/tinker
Discovered Package: laravel/ui
Discovered Package: nesbot/carbon
Discovered Package: nunomaduro/collision
Package manifest generated successfully.
```

Des informations supplémentaire sur la configuration ou les notifications par email [ici](https://github.com/Jorijn/laravel-security-checker).

Pour lancer une recherche:

```bash
$ php artisan security-check:now
Security Check Report: /home/antoinethys/dev/cv-laravel/composer.lock
[OK] 0 packages have known vulnerabilities
```

<a name="repo"></a>

## Repo

Pour la créaction de repository il existe une librairie qui simplifie la tâche.

```bash
$ composer require salmanzafar/laravel-repository-pattern
Using version ^1.0 for salmanzafar/laravel-repository-pattern
./composer.json has been updated
Loading composer repositories with package information
Updating dependencies (including require-dev)
Package operations: 1 install, 0 updates, 0 removals
  - Installing salmanzafar/laravel-repository-pattern (v1.0.1): Downloading (connectDownloading (100%)
Writing lock file
Generating optimized autoload files
> Illuminate\Foundation\ComposerScripts::postAutoloadDump
> @php artisan package:discover --ansi
Discovered Package: binarytorch/larecipe
Discovered Package: facade/ignition
Discovered Package: fideloper/proxy
Discovered Package: jorijn/laravel-security-checker
Discovered Package: laravel/tinker
Discovered Package: laravel/ui
Discovered Package: nesbot/carbon
Discovered Package: nunomaduro/collision
Discovered Package: propaganistas/laravel-phone
Discovered Package: salmanzafar/laravel-repository-pattern
Package manifest generated successfully.

$ php artisan vendor:publish --provider="Salman\RepositoryPattern\RepositoryPatterServiceProvider"
Copied Directory [/vendor/salmanzafar/laravel-repository-pattern/src/resources/stubs] To [/resources/vendor/salmanzafar/stubs]
Publishing complete.
Publishing complete.
```

Pour l'utiliser :

```bash
$ php artisan make:repo User
Repository pattern implemented for model User
```

Les nouveaux fichiers sont créés dans `Repositories/` qui doit être déplacé vers `app/Respositories/`.

Documentation officielle [ici](https://github.com/salmanzafar949/Laravel-Repository-Pattern).
