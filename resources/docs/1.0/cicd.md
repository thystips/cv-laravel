# CI/CD

**Intégration continue avec Gitlab CI.**

---

- [CI/CD](#cicd)
  - [gitlab-ci.yml](#gitlab-ciyml)
  - [.env.testing](#envtesting)

<a name="gitlab-ciyml"></a>

## gitlab-ci.yml

```yml
stages:
  - preparation
  - building
  - testing
  - security

image: edbizarro/gitlab-ci-pipeline-php:7.3

# Variables
variables:
  MYSQL_ROOT_PASSWORD: root
  MYSQL_DATABASE: laravel
  DB_HOST: mysql

cache:
  key: "$CI_JOB_NAME-$CI_COMMIT_REF_SLUG"

composer:
  stage: preparation
  script:
    - php -v
    - composer install --prefer-dist --no-ansi --no-interaction --no-progress --no-scripts
    - cp .env.testing .env
    - php artisan key:generate
  artifacts:
    paths:
      - vendor/
      - .env
    expire_in: 1 days
    when: always
  cache:
    paths:
      - vendor/

yarn:
  stage: preparation
  script:
    - yarn --version
    - yarn install --pure-lockfile
  artifacts:
    paths:
      - node_modules/
    expire_in: 1 days
    when: always
  cache:
    paths:
      - node_modules/

build-assets:
  stage: building
  # Download the artifacts for these jobs
  dependencies:
    - composer
    - yarn
  script:
    - yarn --version
    - yarn run production --progress false
  artifacts:
    paths:
      - public/css/
      - public/js/
      - public/fonts/
      - public/mix-manifest.json
    expire_in: 1 days
    when: always

db-seeding:
  stage: building
  services:
    - name: mysql:8.0
      command: ["--default-authentication-plugin=mysql_native_password"]
  # Download the artifacts for these jobs
  dependencies:
    - composer
    - yarn
  script:
    - mysql --version
    - php artisan migrate:fresh --seed
    - mysqldump --host="${DB_HOST}" --user="root" --password="${MYSQL_ROOT_PASSWORD}" "${MYSQL_DATABASE}" > db.sql
  artifacts:
    paths:
      - storage/logs # for debugging
      - db.sql
    expire_in: 1 days
    when: always

phpunit:
  stage: testing
  services:
    - name: mysql:8.0
      command: ["--default-authentication-plugin=mysql_native_password"]
  # Download the artifacts for these jobs
  dependencies:
    - build-assets
    - composer
    - db-seeding
  script:
    - php -v
    - sudo cp /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini /usr/local/etc/php/conf.d/docker-php-ext-xdebug.bak
    - echo "" | sudo tee /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
    - mysql --host="${DB_HOST}" --user="root" --password="${MYSQL_ROOT_PASSWORD}" "${MYSQL_DATABASE}" < db.sql
    - php artisan config:cache
    - ./vendor/phpunit/phpunit/phpunit --version
    #- sleep 5
    - php -d short_open_tag=off ./vendor/phpunit/phpunit/phpunit -v --colors=never --stderr
    - sudo cp /usr/local/etc/php/conf.d/docker-php-ext-xdebug.bak /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
  artifacts:
    paths:
      - ./storage/logs # for debugging
    expire_in: 1 days
    when: on_failure

codestyle:
  stage: testing
  image: lorisleiva/laravel-docker
  script:
    - phpcs
  dependencies: []

phpcpd:
  stage: testing
  script:
    - test -f phpcpd.phar || curl -L https://phar.phpunit.de/phpcpd.phar -o phpcpd.phar
    - php phpcpd.phar app/ --min-lines=50
  dependencies: []
  cache:
    paths:
      - phpcpd.phar

sensiolabs:
  stage: security
  script:
    - test -d security-checker || git clone https://github.com/sensiolabs/security-checker.git
    - cd security-checker
    - composer install
    - php security-checker security:check ../composer.lock
  dependencies: []
  cache:
    paths:
      - security-checker/
```

<a name="envtesting"></a>

## .env.testing

```env
APP_NAME="cv-template"
APP_ENV=testing
APP_KEY=
APP_DEBUG=true
APP_URL=http://127.0.0.1:8000
LOG_CHANNEL=null
DB_CONNECTION=mysql
DB_HOST=mysql
DB_PORT=3306
DB_DATABASE=laravel
DB_USERNAME=root
DB_PASSWORD=root
BROADCAST_DRIVER=log
CACHE_DRIVER=file
SESSION_DRIVER=file
SESSION_LIFETIME=120
QUEUE_DRIVER=sync
REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379
MAIL_DRIVER=smtp
MAIL_HOST=localhost
MAIL_PORT=1025
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null
PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=
PUSHER_APP_CLUSTER=mt1
MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"
```
