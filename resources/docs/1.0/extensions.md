# Extensions installées

---

-   [Extensions installées](#extensions-install%c3%a9es)
    -   [Laravel](#laravel)
    -   [Spatie](#spatie)
    -   [Barryvdh](#barryvdh) - [_(Dev uniquement)_](#dev-uniquement)

<a name="laravel"></a>

## [Laravel](https://laravel.com/docs/6.x/)

-   [`laravel/ui`](https://laravel.com/docs/6.x/frontend)

<a name="spatie"></a>

## [Spatie](https://docs.spatie.be/)

-   [`spatie/laravel-html`](https://docs.spatie.be/laravel-html/v2/introduction/)
-   [`spatie/laravel-medialibrary`](https://docs.spatie.be/laravel-medialibrary/v7/introduction/)

<a name="ide-helper"></a>

## [Barryvdh](https://github.com/barryvdh)

###### _(Dev uniquement)_

-   [`barryvdh/laravel-ide-helper`](https://github.com/barryvdh/laravel-ide-helper)
