<?php

namespace App;

use Eloquent;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Notifications\DatabaseNotificationCollection;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\Permission\Traits\HasRoles;

/**
 * App\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property int $is_admin
 * @property string $username
 * @property int template_id
 * @property int $custom_cv
 * @property-read Collection|Competences[] $Competences
 * @property-read int|null $competences_count
 * @property-read CustomCv $CustomCv
 * @property-read Collection|Experiences[] $Experiences
 * @property-read int|null $experiences_count
 * @property-read Informations $Informations
 * @property-read Collection|Socials[] $Socials
 * @property-read int|null $socials_count
 * @property-read DatabaseNotificationCollection|DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static Builder|User newModelQuery()
 * @method static Builder|User newQuery()
 * @method static Builder|User query()
 * @method static Builder|User whereCreatedAt($value)
 * @method static Builder|User whereCustomCv($value)
 * @method static Builder|User whereEmail($value)
 * @method static Builder|User whereEmailVerifiedAt($value)
 * @method static Builder|User whereId($value)
 * @method static Builder|User whereIsAdmin($value)
 * @method static Builder|User whereName($value)
 * @method static Builder|User wherePassword($value)
 * @method static Builder|User whereRememberToken($value)
 * @method static Builder|User whereUpdatedAt($value)
 * @method static Builder|User whereUsername($value)
 * @mixin Eloquent
 */
class User extends Authenticatable implements MustVerifyEmail, HasMedia
{
    use Notifiable;
    use HasRoles;
    use HasMediaTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'username', 'custom_cv', 'template_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @return HasOne
     */
    public function Informations(): HasOne
    {
        return $this->hasOne(Informations::class);
    }

    /**
     * @return HasMany
     */
    public function Competences(): HasMany
    {
        return $this->hasMany(Competences::class);
    }

    /**
     * @return HasMany
     */
    public function Experiences(): HasMany
    {
        return $this->hasMany(Experiences::class);
    }

    /**
     * @return HasMany
     */
    public function Socials(): HasMany
    {
        return $this->hasMany(Socials::class);
    }

    /**
     * @return HasOne
     */
    public function CustomCv(): HasOne
    {
        return $this->hasOne(CustomCv::class)->withDefault();
    }

    /**
     * @return string
     */
    public function username(): string
    {
        return $this->username;
    }

    public function template()
    {
        return $this->template_id;
    }

    /**
     * Set the post title.
     *
     * @param string $value
     * @return void
     */
    public function setTemplateAttribute($value): void
    {
        $this->attributes['template_id'] = (int)$value;
    }

    public function registerMediaCollections()
    {
        $this
            ->addMediaCollection('avatar-' . $this->id)
            ->singleFile();
    }
}
