<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

/**
 * App\Socials
 *
 * @property int $id
 * @property string $nom
 * @property string $url
 * @property string $image
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $User
 * @property-read int|null $user_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Socials newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Socials newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Socials query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Socials whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Socials whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Socials whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Socials whereNom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Socials whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Socials whereUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Socials whereUserId($value)
 * @mixin \Eloquent
 */
class Socials extends Model
{

    protected $table = 'socials';
    public $timestamps = true;
    protected $fillable = ['nom', 'url', 'image', 'user_id'];

    public function User(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(User::class);
    }
}
