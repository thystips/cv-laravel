<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

/**
 * App\Competences
 *
 * @property int $id
 * @property string|null $nom
 * @property string|null $description
 * @property int $niveau
 * @property string|null $image
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\User $User
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Competences newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Competences newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Competences query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Competences whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Competences whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Competences whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Competences whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Competences whereNiveau($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Competences whereNom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Competences whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Competences whereUserId($value)
 * @mixin \Eloquent
 */
class Competences extends Model
{

    protected $table = 'competences';
    public $timestamps = true;
    protected $fillable = ['nom', 'description', 'niveau', 'image', 'user_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function User(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
