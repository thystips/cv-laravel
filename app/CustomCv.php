<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

/**
 * App\CustomCv
 *
 * @property int $id
 * @property string|null $link
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\User $User
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CustomCv newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CustomCv newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CustomCv query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CustomCv whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CustomCv whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CustomCv whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CustomCv whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CustomCv whereUserId($value)
 * @mixin \Eloquent
 */
class CustomCv extends Model
{
    protected $table = 'custom_cvs';
    public $timestamps = true;
    protected $fillable = ['link', 'user_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function User(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(User::class);
    }

    /**
     * @return mixed
     */
    public function username()
    {
        return $this->User()->username();
    }
}
