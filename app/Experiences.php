<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

/**
 * App\Experiences
 *
 * @property int $id
 * @property string $nom
 * @property string $titre
 * @property string $date_start
 * @property string|null $date_end
 * @property string|null $description
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\User $User
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Experiences newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Experiences newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Experiences query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Experiences whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Experiences whereDateEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Experiences whereDateStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Experiences whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Experiences whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Experiences whereNom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Experiences whereTitre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Experiences whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Experiences whereUserId($value)
 * @mixin \Eloquent
 */
class Experiences extends Model
{

    protected $table = 'experiences';
    public $timestamps = true;
    protected $fillable = ['nom', 'titre', 'date_start', 'date_end', 'description', 'user_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function User(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
