<?php

namespace App\Http\Controllers;

use App\Template;
use App\User;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;

class UserTemplateController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['show']]);
        $this->middleware('customcv');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        $template = Auth::user()->template();
        return view('cv.user.board.template', ['temp' => $template]);
    }

    /**
     * @param $id
     * @return Factory|View
     */
    public function edit($id)
    {
        $template = Template::all();
        // dd($template);
        return view('cv.user.board.edittemp', ['temps' => $template]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return RedirectResponse|Redirector
     * @throws ValidationException
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'id' => 'required|numeric',
        ]);

        $input = $request->all();
        $user = User::find($id);
        $user->update(['template_id' => $input['id']]);
        return redirect('/board/template')->with('success', 'Template modifié avec succès');
    }
}
