<?php

namespace App\Http\Controllers;

use App\Competences;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;

class CompetencesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['show']]);
        $this->middleware('customcv');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        $comp = Auth::user()->Competences;
        return view('cv.user.board.competences', ['comps' => $comp]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create()
    {
        return view('cv.user.board.addcomp');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse|Redirector
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nom' => 'required|max:255',
            'description' => 'required|max:255',
            'niveau' => 'required|numeric|min:0|max:100',
            'type' => 'boolean',
        ]);

        $user = Auth::user();
        $user->Competences()->save(new Competences($validatedData));

        return redirect('/board/competences')->with('success', 'Compétence crée avec succès');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Factory|View
     */
    public function edit($id)
    {
        $comp = Competences::find($id);
        return view('cv.user.board.editcomp', ['comp' => $comp]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return RedirectResponse|Redirector
     * @throws ValidationException
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nom' => 'required|max:255',
            'description' => 'required|max:255',
            'niveau' => 'required|numeric|min:0|max:100',
            'type' => 'boolean',
        ]);

        $competences = Competences::find($id);
        $input = $request->all();

        $competences->nom = $input['nom'];
        $competences->description = $input['description'];
        $competences->niveau = $input['niveau'];
        // $competences->image = $input['image'];

        $competences->save();

        return redirect('/board/competences')->with('success', 'Compétence sauvegardée avec succès');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return RedirectResponse|Redirector
     * @throws Exception
     */
    public function destroy($id)
    {
        $comp = Competences::find($id);
        $comp->delete();

        return redirect('/board/competences')->with('success', 'Compétence supprimée avec succès');
    }
}
