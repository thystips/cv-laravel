<?php

namespace App\Http\Controllers;

use App\Socials;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SocialController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['show']]);
        $this->middleware('customcv');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $social = Auth::user()->Socials;
        return view('cv.user.board.socials', ['socials' => $social]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('cv.user.board.addsocial');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nom' => 'required|max:255',
            'url' => 'required|max:255',
            'image' => 'required|max:255',
        ]);
        $user = Auth::user();
        $user->Socials()->save(new Socials($validatedData));

        return redirect('/board/socials')->with('success', 'Lien social créé avec succès');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $social = Socials::find($id);
        return view('cv.user.board.editsocial', ['social' => $social]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Validation\ValidationException
     */
    //   public function update(Request $request, $id)
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nom' => 'required|max:255',
            'url' => 'required|max:255',
            'image' => 'required|max:255',
        ]);

        $socials = Socials::find($id);
        $input = $request->all();

        $socials->nom = $input['nom'];
        $socials->url = $input['url'];
        $socials->image = $input['image'];

        $socials->save();

        return redirect('/board/socials')->with('success', 'Lien social sauvegardé avec succès');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function destroy($id)
    {
        $social = Socials::find($id);
        $social->delete();

        return redirect('/board/socials')->with('success', 'Lien social supprimé avec succès');
    }
}
