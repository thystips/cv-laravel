<?php

namespace App\Http\Controllers;

use App\Informations;
use App\User;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;

class InformationsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['show']]);
        $this->middleware('customcv');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        $info = Auth::user()->Informations;
        $avatar = Auth::user()->getMedia('avatar-' . Auth::id());
        return view('cv.user.board.informations', [
            'infos' => $info,
            'avatar' => $avatar
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        // return view('cv.user.board.addinfo');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        // $validatedData = $request->validate([
        //     'prenom' => 'required|max:255',
        //     'nom' => 'required|max:255',
        //     'adresse' => 'nullable|max:255',
        //     'description' => 'required|max:255',
        //     'telephone' => 'nullable|phone:AUTO,FR,BE,US',
        //     'image' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
        // ]);

        // $user = Auth::user();
        // $user->Informations()->save(new Informations($validatedData));

        // return redirect('/board/informations')->with('success', 'Informations sauvegardées avec succès');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        if ($id != Auth::id()) {
            return redirect()->route('editinfo', ['id' => Auth::id()]);
        } else {
            $info = User::find($id)->Informations;
            return view('cv.user.board.editinfo', ['infos' => $info]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return RedirectResponse|Redirector
     * @throws ValidationException
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\DiskDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileIsTooBig
     */
    public function update(Request $request, $id)
    {
        //        $fileDest = 'users/' . Auth::User()->username();
        //        $fileName = 'avatar.pdf';

        $this->validate($request, [
            'avatar' => 'image|mimes:jpeg,png,jpg,gif',
            'prenom' => 'required|max:255',
            'nom' => 'required|max:255',
            'email' => 'max:255',
            'adresse' => 'max:255',
            'description' => 'required|max:255',
            'telephone' => 'phone:AUTO,FR,BE,US',
        ]);

        $informations = Informations::find($id);
        $input = $request->all();

        //        $path = $input['avatar']->file('avatar')->storeAs($fileDest, $fileName);

        if (isset($input['avatar'])) {
            $avatar = Auth::user()->addMediaFromRequest('avatar');
            $avatar->setName('avatar-' . Auth::id());
            $avatar->setFileName('avatar.png');
            $avatar->withResponsiveImages();
            $avatar->toMediaCollection('avatar-' . Auth::id());
        }

        $informations->prenom = $input['prenom'];
        $informations->nom = $input['nom'];
        $informations->email = $input['email'];
        $informations->adresse = $input['adresse'];
        $informations->description = $input['description'];
        $informations->telephone = $input['telephone'];
        //        $informations->image = $path;


        $informations->save();

        return redirect('/board/informations')->with('success', 'Informations modifiées avec succès');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return RedirectResponse|Redirector
     */
    public function destroy($id)
    {
        // $info = Informations::find($id);
        // $info->delete();
        $informations = Informations::find($id);

        $informations->prenom = NULL;
        $informations->nom = NULL;
        $informations->email = NULL;
        $informations->adresse = NULL;
        $informations->description = NULL;
        $informations->telephone = NULL;
        $informations->image = NULL;

        $informations->save();

        return redirect('/board/informations')->with('success', 'Information supprimée avec succès');
    }
}
