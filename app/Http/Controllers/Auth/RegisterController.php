<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make(
            $data,
            [
                'name' => ['required', 'alpha_spaces', 'max:255'],
                'username' => ['required', 'string', 'max:255', 'unique:users', 'alpha_dash'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:8', 'confirmed'],
            ],
            [
                'name.required' => 'Vous devez spécifier un nom',
                'name.alpha_spaces' => 'Votre nom ne peut pas comporter des caractères spéciaux ou des nombres',
                'name.max:255' => 'Votre nom est trop long (MAX : 255 caractères)',

                'username.required' => 'Vous devez spécifier un nom d\'utilisateur',
                'username.string' => 'Votre nom d\'utilisateur ne peut pas être seulement des nombres ou caractères spéciaux',
                'username.max' => 'Votre nom d\'utilisateur est trop long (MAX : 255 caractères)',
                'username.unique' => 'Votre nom d\'utilisateur est déjà pris',
                'username.alpha_dash' => 'Votre nom d\'utilisateur ne peut être composé que de lettre et chiffres',

                'email.required' => 'Vous deviez spécifier un email',
                'email.string' => 'L\'adresse email n\'est pas valide',
                'email.email' => 'Vous devez spécifier une adresse email valide',
                'email.max' => 'L\'adresse email est trop longue',
                'email.unique' => 'L\'adresse email est déjà utilisée',

                'password.required' => 'Vous devez spécifier un mot de passe',
                'password.string' => 'Votre mot de passe doit être plus varié !',
                'password.min' => 'Votre mot de passe est trop court',
                'password.confirmed' => 'Les deux mots de passes ne correspondent pas',

            ]
        );
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return User
     */
    protected function create(array $data): User
    {
        $user = User::create([
            'name' => $data['name'],
            'username' => $data['username'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);

        // Add role
        $user->assignRole('user');

        // Add blank records
        $user->Informations()->create([]);
        $user->CustomCv()->create([]);

        // Create folder for storage
        $dir = 'users/' . $user->username();
        Storage::makeDirectory($dir);

        // Create collection for avatar
        $user->registerMediaCollections();

        return $user;
    }
}
