<?php

namespace App\Http\Controllers;

use App\CustomCv;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Response;

class CustomCvController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['show']]);
        $this->middleware('verified');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $customCv = Auth::user()->CustomCv;
        return view('cv.user.board.customcv', [
            'customCv' => $customCv,
            'path' => 'users/' . Auth::User()->username() . '/custom.pdf'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return string
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function show($id)
    {
        if (Auth::user()->custom_cv) {
            $customCv = User::find($id)->CustomCv->link;
            $filePath = $customCv;

            $fileContent = Storage::get($filePath);

            $type = Storage::mimeType($filePath);

            return Response::make($fileContent, 200, [
                'Content-Type'        => $type,
                'Content-Disposition' => 'inline; filename="custom.pdf"'
            ]);
        } else {
            $cvcontroller = new CvController();
            return $cvcontroller->show($id);
        }
    }

    /**
     * Download the specified resource.
     *
     * @param int $id
     * @return file
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function download($id)
    {
        if ($id == Auth::id()) {
            $customCv = User::find($id)->CustomCv;
            return Storage::download($customCv->link, 'CV.pdf');
        } else {
            return redirect('/board/customcv');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $customcv = CustomCv::find($id);
        return view('cv.user.board.editcustomcv', ['customcv' => $customcv]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $fileDest = 'users/' . Auth::User()->username();
        $fileName = 'custom.pdf';

        $request->validate([
            'customcv' => 'required|mimes:pdf',
        ]);

        $path = $request->file('customcv')->storeAs($fileDest, $fileName);
        $customCv = User::with('CustomCv')->find($id)->CustomCv();
        $customCv->update(['link' => $path]);

        return redirect('/board/customcv')->with('success', 'CV personnalisé ajouté avec succès');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $customCv = User::with('CustomCv')->find($id)->CustomCv();

        $customCv->update(['link' => NULL]);

        $file = 'users/' . Auth::User()->username() . '/custom.pdf';

        Storage::delete($file);

        return redirect('/board/customcv')->with('success', 'CV personnalisé supprimé avec succès');
    }
}
