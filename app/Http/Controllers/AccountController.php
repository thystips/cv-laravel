<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class AccountController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $user = Auth::user();

        return view('cv.user.account.accountinfos', ['user' => $user]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if ($id != Auth::id()) {
            return redirect()->route('editaccount', ['id' => Auth::id()]);
        } else {
            $user = User::find($id);
            return view('cv.user.account.editaccountinfos', ['user' => $user]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $input = $request->all();
        $validation = ['name' => ['required', 'alpha_spaces', 'max:255']];

        if (!isset($input['custom_cv'])) {
            $input['custom_cv'] = 0;
        }

        if ($input['email'] === $user->email) {
            $validation += ['email' => 'required|string|email|max:255'];
        } else {
            $validation += ['email' => 'unique|required|string|email|max:255'];
        }

        if ($input['username'] === $user->username) {
            $validation += ['username' => 'required|string|max:255|alpha_dash'];
        } else {
            $validation += ['username' => 'unique|required|string|max:255|alpha_dash'];
        }

        $this->validate($request, $validation);

        $user->name = $input['name'];
        $user->username = $input['username'];
        $user->email = $input['email'];
        $user->custom_cv = $input['custom_cv'];

        $user->save();

        return redirect('/account')->with('success', 'Account modifié avec succès');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function UserLogout()
    {
        Auth::logout();
        return Redirect::route('index');
    }

    public function getUserBoard()
    {
        if (Auth::User()->custom_cv === True) {
            return redirect('/board/customcv');
        }

        return redirect('/board/competences');
    }
}
