<?php

namespace App\Http\Controllers;

use App\Experiences;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ExperiencesController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth', ['except' => ['show']]);
    $this->middleware('customcv');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
   */
  public function index()
  {
    $exp = Auth::user()->Experiences;
    return view('cv.user.board.experiences', ['exps' => $exp]);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
   */
  public function create()
  {
    return view('cv.user.board.addexp');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
   */
  public function store(Request $request)
  {


    $validatedData = $request->validate([
      'nom' => 'required|max:255',
      'titre' => 'required|max:255',
      'date_start' => 'required|date',
      'date_end' => 'nullable|date',
      'description' => 'nullable|max:255',
    ]);

    $user = Auth::user();
    $user->Experiences()->save(new Experiences($validatedData));

    return redirect('/board/experiences')->with('success', 'Expériences crée avec succès');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
   */
  public function edit($id)
  {
    $exp = Experiences::find($id);
    return view('cv.user.board.editexp', ['exp' => $exp]);
  }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Validation\ValidationException
     */
  public function update(Request $request, $id)
  {
    $this->validate($request, [
      'nom' => 'required|max:255',
      'titre' => 'required|max:255',
      'date_start' => 'required|date',
      'date_end' => 'nullable|date',
      'description' => 'nullable|max:255',
    ]);

    $experiences = Experiences::find($id);
    $input = $request->all();

    $experiences->nom = $input['nom'];
    $experiences->titre = $input['titre'];
    $experiences->date_start = $input['date_start'];
    $experiences->date_end = $input['date_end'];
    $experiences->description = $input['description'];

    $experiences->save();


    return redirect('/board/experiences')->with('success', 'Expérience sauvegardé avec succès');
  }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
  public function destroy($id)
  {
    $exp = Experiences::find($id);
    $exp->delete();

    return redirect('/board/experiences')->with('success', 'Expérience supprimée avec succès');
  }
}
