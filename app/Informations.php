<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

/**
 * App\Informations
 *
 * @property int $id
 * @property string|null $prenom
 * @property string|null $nom
 * @property string|null $email
 * @property string|null $adresse
 * @property string|null $description
 * @property string|null $telephone
 * @property string|null $image
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\User $User
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Informations newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Informations newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Informations query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Informations whereAdresse($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Informations whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Informations whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Informations whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Informations whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Informations whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Informations whereNom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Informations wherePrenom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Informations whereTelephone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Informations whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Informations whereUserId($value)
 * @mixin \Eloquent
 */
class Informations extends Model
{

    protected $table = 'informations';
    public $timestamps = true;
    protected $fillable = ['prenom', 'nom', 'adresse', 'description', 'telephone', 'image', 'user_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function User(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(User::class);
    }
}
